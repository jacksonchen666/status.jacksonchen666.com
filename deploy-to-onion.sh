#!/bin/sh
set -eu

hugo --environment tor-onion --destination public-tor
tar -C public-tor/ -cz . > public-tor.tar.gz
ssh -v -o VerifyHostKeyDNS=yes deploy@server -T "status.jacksonchen666.com::onion" < public-tor.tar.gz
rm public-tor.tar.gz
rm -r public-tor/
