Add or replace these in the front matter:

```yaml
date: somefuturedatehere
#resolved: false
informational: true
pin: true
```

Yes, the `resolved: false` should be commented out.

Adding `pin: true` is suggested to give visibility, as incidents history is
located way below a bunch of services.

When starting a scheduled thing, replace values with something like these in
the front matter:

```yaml
date: samedateasbefore
resolved: false
pin: true
```

(`pin: true` should be removed when an issue/maintenance is
resolved/finished)

(See <https://github.com/cstate/cstate/wiki/Usage#announcing-maintenance-informational-posts-pinned-or-not-v52>)
