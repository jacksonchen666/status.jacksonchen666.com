To create an item in the status thingy that is maintenance instead of
downtime, replace `severity` in the front matter with this:

```yaml
severity: maintenance
```

If this is a scheduled maintenance, see <./scheduling.md>.
