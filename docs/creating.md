To create an incident (maintenance or otherwise), run this:

```
hugo new content/issues/$(date -Id)-title-here-may-include-hostnames.md
```

Then edit the file and its front matter.
