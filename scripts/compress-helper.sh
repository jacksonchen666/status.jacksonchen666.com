#!/bin/sh

# Copyright 2024 JacksonChen666
#
# SPDX-License-Identifier: MIT

set -eu

usage() {
    echo "usage: $0 <brotli|gzip> <filename>" >&2
    exit 1
}

get_file_size() {
    wc -c < "$1"
    # :P
    # https://stackoverflow.com/questions/1815329/portable-way-to-get-file-size-in-bytes-in-the-shell/#1815582
}

remove_if_badly_compressed() {
    compressed_file_name="$1"
    original_file_name="$2"
    compressed_size="$(get_file_size "$compressed_file_name")"
    original_size="$(get_file_size "$original_file_name")"
    ten_percent_of_original_size="$((original_size - (original_size / 10)))"
    # less than 10% compressed compared to original size
    if [ "$compressed_size" -gt "$ten_percent_of_original_size" ]; then
        #echo "rm $compressed_file_name ($compressed_size > $ten_percent_of_original_size (90% is $original_size))" &&
        rm "$compressed_file_name"
    fi
}

[ "$#" -ne 2 ] && usage

if [ "$1" = "brotli" ]; then
    if [ ! -f "$2.br" ]; then
        brotli -Z -- "$2"
    fi
    remove_if_badly_compressed "$2.br" "$2"
elif [ "$1" = "gzip" ]; then
    if [ ! -f "$2.gz" ]; then
        gzip -k9 -- "$2"
    fi
    remove_if_badly_compressed "$2.gz" "$2"
else
    usage
fi
