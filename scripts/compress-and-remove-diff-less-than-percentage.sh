#!/bin/sh

# Copyright 2024 JacksonChen666
#
# SPDX-License-Identifier: MIT

# it's basically the following:
#gzip_output=$(gzip -9rkqv $stage/ 2>&1)
#rm -f $(awk --posix '$2 * 1 < 10 {print $5}' <<< $gzip_output) || true # does it really matter that it errors?
#
# but hopefully in a more posix way
set -eu

usage() {
    echo "usage: $0 <path_to_directory>" >&2
    exit 1
}

[ "$#" -eq "1" ] || usage

dirname="$(dirname "$0")"

folder_to_compress_files_individually="$1"

alias find_compressible='find "$folder_to_compress_files_individually" -type f \! \( -name "*.gz" -o -name "*.br" \)'

find_compressible | xargs -n1 -P "$(nproc)" sh "$dirname"/compress-helper.sh brotli
find_compressible | xargs -n1 -P "$(nproc)" sh "$dirname"/compress-helper.sh gzip
