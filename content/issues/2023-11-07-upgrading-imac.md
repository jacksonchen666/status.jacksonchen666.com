---
title: "Upgrading imac"
date: 2023-11-07T08:05:00Z
resolved: true
resolvedwhen: 2023-11-07T08:25:00Z
# You can use: down, disrupted, notice
severity: notice
affected:
- blackbox prober
section: issue
---
