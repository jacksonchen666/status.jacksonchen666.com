---
title: planned downtime (black out)
date: 2022-09-15T09:00:00+02:00
resolved: true
resolvedWhen: 2022-09-15T09:00:00+02:00
# You can use: down, disrupted, notice
severity: notice
affected:
- matrix homeserver
- matrix chat client
- mastodon
- files
- rsync
- analytics/tracking
- uptime kuma
- videos
- main website over onion
- onion logs
- files onion
section: issue
informational: true
---
due to a future planned black out for reasons unrelated to the server, the server will not be able to connect to the internet and it will not be able to serve subdomain services during the said times.
the downtime may increase in an unexpected manner.

I AM NOT IN CONTROL OF THE BLACK-OUTS.

# update 2022-09-15T13:17:02+02:00 - nothing happened
nothing happened. no black-out happened.
i don't know why, but there was no issue of the server disconnecting.
