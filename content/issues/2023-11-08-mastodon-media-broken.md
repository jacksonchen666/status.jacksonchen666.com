---
title: "Mastodon Media Broken"
date: 2023-11-07T21:45:00Z
resolved: true
resolvedWhen: 2023-11-08T00:50:00+00:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- mastodon
section: issue
---
# noticed 2023-11-08T00:02:08+00:00

I noticed an issue with media:

It's gone.

Media will be restored soon. Please wait.

# update 2023-11-08T01:30:08+01:00 - temporary shutdown

The instance will be temporally shutdown until we can get the load under
control.

# update 2023-11-08T00:50:00+00:00 - fixed

Fixed Mastodon media.
