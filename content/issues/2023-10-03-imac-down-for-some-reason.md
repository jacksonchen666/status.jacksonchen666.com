---
title: "imac Down for Some Reason"
date: 2023-10-03T10:20:00Z
resolved: true
resolvedWhen: 2023-10-06T15:50:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
- blackbox prober
section: issue
---
Possible temporary power outage killing imac.

Down until 2023-10-06T15:50:00+00:00.
