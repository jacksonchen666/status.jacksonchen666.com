---
title: "Synapse Broke"
date: 2024-09-04T11:05:00Z
resolved: true
resolvedWhen: 2024-09-04T12:00:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
section: issue
---
# resolved 2024-09-04T12:00:00+00:00

Synapse has been fixed. We just threw away the Python virtual environment
and started from scratch. It works. We have no idea why.
