---
title: broken mastodon
date: 2022-11-07T22:00:00+01:00
resolved: true
resolvedWhen: 2022-11-07T23:09:55+01:00
# You can use: down, disrupted, notice
severity:
- down
affected:
- mastodon
section: issue
---
i tried to upgrade to [v4.0.0rc2](https://github.com/mastodon/mastodon/releases/tag/v4.0.0rc2) (also account for [v4.0.0rc1](https://github.com/mastodon/mastodon/releases/tag/v4.0.0rc1)) and i broken mastodon.

again, same issue with too-new openssl and legacy stuff being not provided/working. which is annoying.

the problem i'm having is that the compiling of the assets do not work because of openssl 3.

i am trying to fix this right now.
data shouldn't be lost in case a restore is needed.

# update 2022-11-07T23:09:55+01:00 - problem solved
so basically, [apply this](https://stackoverflow.com/questions/72866798/node-openssl-legacy-provider-is-not-allowed-in-node-options/73064710#73064710) before running the compile step, then revert the config and start it again.

it now works.

that's all.
