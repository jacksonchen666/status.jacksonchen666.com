---
title: "I2p Site Down"
date: 2024-05-19T21:30:00Z
resolved: true
resolvedWhen: 2024-05-19T22:05:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
- i2p site
section: issue
---
# resolved 2024-05-19T22:05:00+00:00

Due to some human error (going "Yes!" on an uninstall procedure which
happened to include i2pd), i2pd got uninstalled.

We already reinstalled it, but forgot to fix the configuration. The downtime
should be resolved now.
