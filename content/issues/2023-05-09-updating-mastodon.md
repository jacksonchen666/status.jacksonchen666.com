---
title: updating mastodon
date: 2023-05-09T21:47:34+02:00
resolved: true
resolvedWhen: 2023-05-09T21:52:23+02:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- mastodon
section: issue
---
