---
title: "Sourcehut Pages Not Working"
date: 2024-02-27T02:45:00Z
resolved: true
resolvedWhen: 2024-02-27T08:25:00Z
# You can use: down, disrupted, notice
severity: down
affected:
- main website backup
- status pages
section: issue
---
Gemini backup site not affected.
