---
title: i2p site not reachable
date: 2023-04-30T09:04:03+02:00
resolved: true
resolvedWhen: 2023-04-30T09:26:15+02:00
# You can use: down, disrupted, notice
severity: down
affected:
- i2p site
section: issue
---
# 2023-04-30T09:26:51+02:00 - issue noticed

# 2023-04-30T09:28:15+02:00 - issue resolved on its own

the i2p site seems to be reachable again
