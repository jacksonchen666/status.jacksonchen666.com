---
title: mastodon broken
date: 2022-05-11T10:22:53+02:00
resolved: false
section: issue
severity: down
affected:
- mastodon
resolved: true
resolvedWhen: 2022-06-28T15:39:42+02:00
---
hello, my mastodon instance is down because it's broken.

if you would like to help solve my problem, [see more details here](https://github.com/mastodon/mastodon/discussions/18392).

otherwise, it is not working and i don't have a solution for that.

# update 2022-06-28T15:39:42+02:00 - it now works with some changes
by changes i mean using [this fork](https://github.com/ClearlyClaire/mastodon/tree/fixes/webauthn-openssl-3) from [this pr](https://github.com/mastodon/mastodon/pull/18449) after finally taking my chance, and now it actually works.

now i do have other problems to solve, like ports, otherwise federation won't really work (except for my matrix HS because federation is on a working port).
