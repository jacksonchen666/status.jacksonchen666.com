---
title: "Planned Downtime for imac"
date: 2023-09-28T17:00:00Z
resolved: true
resolvedWhen: 2023-09-28T18:10:00Z
# You can use: down, disrupted, notice
severity: notice
affected:
- blackbox prober
section: issue
---
imac will be down for some maintenance work. This is estimated to take
half an hour, or 2 hours at most.

# update 2023-09-28T17:45:00Z

We are using up our later deadline, so this will be resolved at most 2 hours
after initial announced time.

# update 2023-09-28T18:10:00Z

it is done. that's all.
