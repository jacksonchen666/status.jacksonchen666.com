---
title: Temporary downtime on server
date: 2021-12-15T17:54:00+00:00
resolved: true
resolvedWhen: 2021-12-17T15:37:08+01:00
severity: down
section: issue
affected:
- matrix homeserver
- matrix chat client
- new blog
- files
---
edit: no more downtime. also, i won't be editing posts like this when it's done.
If you have been redirected here when trying to access any of my services, they are down right now and it is planned to be down for a bit while (give or take 1 hour, no guarantees).

The downtime has nothing to do with the actual server, except it's internet connection which will be disconnected for a bit of time.

This downtime should not last longer than 6 hours since publication of this post.
