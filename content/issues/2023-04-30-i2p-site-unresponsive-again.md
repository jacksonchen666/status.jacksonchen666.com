---
title: i2p site unresponsive again
date: 2023-04-30T11:29:13+02:00
resolved: true
resolvedWhen: 2023-05-07T08:55:22+02:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- i2p site
section: issue
---
# 2023-04-30T14:51:23+02:00 - issue renoticed

issue was ignored as it had repeated, but it seems like it had persisted

# 2023-04-30T15:22:53+02:00 - I2P router DDoS?

a restart was attempted, but it doesn't seem to have done much.

some info:
- tunnel creation success rate: 0% (minimum 10% for functioning)
- pretty high received and sent traffic (although i should note, the server's
  i2p router is a floodfill router which has to deal with a lot of traffic)
- 0 inbound/outbound tunnels for the website thingy

# 2023-04-30T15:58:13+02:00 - still can't figure out the issue

i tried some stuff, still can't figure out the issue.

# 2023-05-01T12:38:17+02:00 - it's a (D)DoS

according to [a
comment](https://github.com/PurpleI2P/i2pd/issues/1915#issuecomment-1529562892),
my i2pd router can be considered DDoSed.

possibly due to that, that makes the i2p site unavailable.

the i2pd router is currently set to not accept any transit tunnels (for no
good reason).

# 2023-05-02T15:15:20+02:00 - almost fine?

since around 2023-05-02T12:42:23+02:00, it seems like uptime has only been
spotty instead of very down.

since it's still spotty, i wouldn't consider this resolved yet.

# 2023-05-03T18:26:39+02:00 - updated to disrupted

this incident has been updated to disrupted because it's mostly disrupted
and not down.

still no idea what to do

# 2023-05-07T08:55:22+02:00 - gonna call it end

i'm going to call it resolved now because issue does not seem to be there
anymore
