---
title: "imac Having Downtimes"
date: 2024-01-03T12:35:00Z
resolved: true
resolvedWhen: 2024-01-03T14:45:00Z
# You can use: down, disrupted, notice
severity: down
affected:
- blackbox prober
section: issue
---
