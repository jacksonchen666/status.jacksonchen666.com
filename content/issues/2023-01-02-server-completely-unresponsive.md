---
title: server completely unresponsive
date: 2023-01-02T22:15:14+01:00
resolved: true
resolvedWhen: 2023-01-02T22:36:26+01:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
- matrix chat client
- mastodon
- files
- uptime kuma
- videos
- main website over onion
- files onion
- status onion
section: issue
---
the server has been not been responding for long for me to say the server is
not responding.

i tried physical access, but that wasn't successful since not even the tty
would respond.

also, the disk indicator light is barely blinking, so there is something
else going on.

i will be rebooting the server. but because it's not responding, here's my
steps:
1. disconnect the server from the internet
2. try to login
3. (if login succeeds) try `poweroff` (maybe with `-f`)

if login doesn't succeed, the server will be force shutdown.

# update 2023-01-02T22:28:58+01:00 - force shutdown
the server is responding to sysrq requests by saying it has been disabled.

anyways, it has been decided to force shutdown the server.

# end 2023-01-02T22:36:26+01:00
the server has been restarted and it is not working

# addition 2023-01-02T23:07:12+01:00 - analysis
i was looking at prometheus and it seemed like everything stopped at
2023-01-02T22:04:28+01:00 (or rather, prometheus was unable to scrape and
save data).

at 2023-01-02T22:36:28+01:00, the first scrape after the reboot was recorded.

why it happened was probably caused by the fact that i was doing some lvm
operation, then it just took forever to do it and never finished it.
