---
title: "Upgrading imac"
date: 2023-10-28T20:40:00Z
resolved: true
resolvedWhen: 2023-10-28T21:00:00Z
# You can use: down, disrupted, notice
severity: down
affected:
- blackbox prober
section: issue
---
Maybe an hour, 2 hours max.
