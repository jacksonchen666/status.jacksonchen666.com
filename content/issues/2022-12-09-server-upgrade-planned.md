---
title: server upgrade planned
date: 2022-12-09T19:00:00+01:00
resolved: true
resolvedWhen: 2022-12-09T19:12:20+01:00
# You can use: down, disrupted, notice
severity: notice
affected:
- matrix homeserver
- matrix chat client
- mastodon
- files
- rsync
- analytics/tracking
- uptime kuma
- videos
- postgresql database
- main website over onion
- files onion
- status onion
section: issue
---
fedora 37 was already released, but i'm just getting around to upgrading the server for it.

on the set time, the server upgrade will start.

plan:
- the server is fully upgraded (as per request by `dnf system-upgrade`, ? minutes)
- the server will download the updates (0-5 minutes)
- the server will install the updates offline (30-60 minutes?)
    - during that time, the server will be down

# start - server upgrading
the packages has been downloaded.

the reboot will happen on the set time, which will result in services being down.

after the reboot, fedora will boot into an environment where it will upgrade all the packages.
then, it will reboot, then go back to normal operation.

this update will be posted when the upgrade has started.

soft time limit: 30 minutes.

hard time limit: 2 hours.

# update 2022-12-09T19:13:02+01:00 - it seems to be finished
it appears that the server upgrade is finished.

first logs from node exporter was 2022-12-09T19:12:20+01:00 so that will be the resolved time.
