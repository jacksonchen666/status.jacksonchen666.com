---
title: Power outage cutting off all services until further notice
section: issue
severity: down
affected:
- all
date: 2022-02-01 06:31:36 +0100
resolved: true
resolvedWhen: 2000-01-01T00:00:00+00:00
---
lucky to have internet here, because the power went out at around 05:30+0100 and it's actually hard to get access to the internet because of the power outage.

all services are disrupted. they should restore today if the power does restore (which it did while at 06:33 +0100 but died again).

i apologize for the inconvenience, but there's nothing i can actually do about this.

UPDATE 2022-02-01 06:51 +0100: power restored, but died again.
