---
title: "imac Having Downtimes"
date: 2024-01-02T23:10:00Z
resolved: true
resolvedWhen: 2024-01-03T04:10:00Z
# You can use: down, disrupted, notice
severity: down
affected:
- blackbox prober
section: issue
---
