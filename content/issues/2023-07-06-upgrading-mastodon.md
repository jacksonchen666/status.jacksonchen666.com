---
title: "Upgrading Mastodon"
date: 2023-07-06T14:40:00+00:00
resolved: true
resolvedWhen: 2023-07-06T14:55:00+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
- mastodon
section: issue
---
Mastodon will be shutdown temporally for this upgrade.
