---
title: "Server IP Got Removed"
date: 2023-11-16T03:47:00Z
resolved: true
resolvedWhen: 2023-11-16T06:56:52+00:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- alertmanager
- files
- jc6.xyz
- jc666.xyz
- main gemini capsule
- main website
- mastodon
- matrix homeserver
- prometheus
- uptime status
- videos
section: issue
---
# update 2023-11-16T07:40:14+00:00 - post mortem

So what happened is the following:
- ddclient tries to update IP addresses
- It tries to get an IP address
- Trying to get an IP address times out
- ddclient updates the IP to nothing
- Cascading failure due to missing IPs
- Everything breaks

This did not break other services which did not rely on DNS.

# update 2023-11-16T07:54:56+00:00 - meta status website

To clarify what is broken and what isn't, we have added more services that
are being hosted over onion.

This is done is because the onions didn't break at all.
