---
title: "Imac System Upgrade"
date: 2023-09-23T20:15:00Z
resolved: true
resolvedWhen: 2023-09-23T20:30:00+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
- blackbox prober
section: issue
---
The imac (hosts blackbox prober which checks uptime of service) will be
temporally down a bit for a system upgrade.

Uptime status at <https://uptime.jacksonchen666.com> will be incorrect.
