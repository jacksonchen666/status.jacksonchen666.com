---
title: mastodon sidekiq nonfunctional
date: 2022-12-09T19:55:47+01:00
resolved: true
resolvedWhen: 2022-12-09T19:58:48+01:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- mastodon
section: issue
---
> No Sidekiq process running for the default, push, mailers, pull, scheduler, ingress queue(s). Please review your Sidekiq configuration

that's on the mastodon admin dashboard, i wonder what's going on

# update 2022-12-09T19:58:48+01:00 - found out why
found out why, `mastodon-sidekiq.service` was not running because related to the previous mastodon issue.
