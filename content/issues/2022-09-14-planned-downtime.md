---
title: planned downtime (black out)
date: 2022-09-14T09:00:00+02:00
resolved: true
resolvedWhen: 2022-09-14T18:31:05+02:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
- matrix chat client
- mastodon
- files
- rsync
- analytics/tracking
- uptime kuma
- videos
- main website over onion
- onion logs
- files onion
section: issue
---
due to a future planned black out for reasons unrelated to the server, the server will not be able to connect to the internet and it will not be able to serve subdomain services during the said times.
the downtime may increase in an unexpected manner.

I AM NOT IN CONTROL OF THE BLACK-OUTS.

# update 2022-09-14T18:31:05+02:00 - power restored
power has been restored and the server has been already started.
