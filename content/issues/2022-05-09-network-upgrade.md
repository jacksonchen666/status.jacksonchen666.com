---
title: network upgrade
date: 2022-05-09T19:16:33+02:00
resolvedWhen: 2022-05-11T12:34:56+02:00
resolved: true
severity: disrupted
section: issue
---
oh hey hi again, i'm just here to say downtime is going to happen again, except this time it's another hardware upgrade and it's the router.

the router is being changed out, and hopefully it will work first hand.
if not, i'll just have to revert until everything works.

# times
- starting from 2022-05-09T19:20:00+02:00
- soft limit: 1.5 hours
- hard limit: 3 hours
- affected services
    - all subdomain services (that doesn't include microblogging)
    - all onion services (that does include microblogging)
    - all other undocumented services also hosted by me

# how it will affect me
this will affect me, which is i will be unable to access the internet until i get the things working.

you can still email me, but i will not be on matrix since my matrix homeserver is affected.

# update 2022-05-09T21:17:04+02:00 - network upgrade status
the upgrade status of the network is **unsuccessful**.

while trying to upgrade, there were problems of trying to get the router to work with the connection.
unfortunately, it seems like the router that was received is unable to both connect to the supposed "required router" and replacing the "required router", as the critical component for connecting to the network appears to be proprietary/not available for the new received router.

however, it sounds like a technician will come and try to get the router working in 48/72 hours, so *maybe* expect downtime that can only be fixed physically, which the rest i won't be able to fix immediately unfortunately.

result: no changes has been made.

# update 2022-05-11T12:34:56+02:00 - technician network upgrade
i said a technician will come and try and get it working.
and boy did they do so without me noticing that it happened (not implying that they got in without me knowing, since i wasn't there anyways).

since the new router doesn't have all the ports open from the previous router, i will have to set it up, which i'm in the process of doing so.

expect all subdomain and undocumented services to be distrusted until further notice or when it's fixed.

# update 2022-05-11T13:40:40+02:00 - further progress, but not complete
some ports has been opened, but not all of them.
one of them is 8448, which means my matrix server can be access, but that's it.
it's now slow.

also, i really should learn my lesson [last time]({% post_url 2022-03-15-wait-why-is-port-53-open-on-my-server %}), but i didn't.

# update 2022-05-11T18:38:45+02:00 - new dedicated post
because it has become a big issue now, a new maintenance post will be issued.

please see [this post](/maintenance/2022-05-11/16-40-54/)
