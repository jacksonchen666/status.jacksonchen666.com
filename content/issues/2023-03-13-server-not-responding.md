---
title: "server not responding"
date: 2023-03-13T11:15:38+01:00
resolved: true
resolvedWhen: 2023-03-14T10:58:53+01:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- matrix homeserver
- matrix chat client
- mastodon
- files
- uptime kuma
- videos
- prometheus
- alertmanager
- grafana
- main website over onion
- files onion
- status onion
section: issue
---
from what i can tell, the network is down.

starting time is estimated.

# update 2023-03-14T07:39:25+01:00 - further information

further information has been received.

apparently, the cable connecting to the ISP may have been physically broken.

services may be down for an extended amount of time because of that.

# update 2023-03-14T09:59:33+01:00 - server was back online for a bit?

so i looked at my emails and saw that i got alertmanager emails for a bit.

i'm not sure when it went down again, but it's very likely that it was quite
recent.

start time is quite close to first alert of network down issue.

# update 2023-03-14T12:21:29+01:00 - it's over?

server is up and reachable, will be analyzing situation and maybe figure out
why things happened.

end date will not be set yet. it might've went down again on
2023-03-14T12:22:51+01:00.

# update 2023-03-14T16:11:45+01:00 - start and end updated

the start and end time has been updated to match what it is saying in
[prometheus](https://prometheus.jacksonchen666.com/graph?g0.expr=node_bonding_active%7Binstance%3D%22laptop-server%22%2Cmaster%3D%22bond0%22%7D%20%3D%3D%200&g0.tab=0&g0.stacked=0&g0.show_exemplars=0&g0.range_input=2d).

there has been a small bit of time where it was online again
(2023-03-14T09:23:53+01:00), but it did
went offline again (2023-03-14T09:47:53+01:00).

