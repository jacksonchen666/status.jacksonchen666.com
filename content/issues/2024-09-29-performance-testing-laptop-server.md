---
title: "Performance Testing laptop-server"
date: 2024-09-29T19:00:00Z
resolved: true
resolvedWhen: 2024-09-29T23:30:00+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
##server
- main website
- main gemini capsule
- files
- jc6.xyz
- jc666.xyz
- mastodon
- matrix homeserver
- prosody
- reinfo wiki
- videos
- wiki
- status pages (laptop-server)
- uptime status
- i2p site
- files onion
- main website over onion
- mastodon onion
- reinfo wiki onion
- status onion
- uptime status onion
- wiki onion
- main website over yggdrasil
- calendar
- miniflux
- ntfy
- gts.hazmat.jacksonchen666.com
- another-gts.hazmat.jacksonchen666.com
- mumble server
- croc
##imac
#- blackbox prober
##sourcehut pages
#- status pages
#- main website backup
#- main gemini capsule backup
section: issue
pin: true
---
A disk performance test will happen on laptop-server, which will take an
unknown amount of time. Performance will suffer, and another test will
happen where all services will be shut off.

This will start at the marked time.

# update 2024-09-29T19:00:00+00:00

The test has started. When the test is finished, all laptop-server services
will be shut off for another test.

# update 2024-09-29T20:00:00+00:00 - shutting things off

The online test has finally completed. Services will now be shut off and
another test will happen.

# update 2024-09-29T20:35:00+00:00 - partial uptime

Some services have been brought back online for another test.

# update 2024-09-29T21:10:00+00:00 - more partial uptime and testing

Further testing is happening, with starting few services at a time
incrementally, to determine which programs take up most of the I/O time and
throughput.

These tests can take up to multiple hours to run, and will definitely affect
server performance.

# finished 2024-09-29T23:30:00+00:00

The tests have been finished. All services restored and normal (suboptimal)
performance should return.
