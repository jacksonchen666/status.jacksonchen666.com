---
title: Domain transfer in progress
date: 2023-04-10T19:34:48+02:00
resolved: true
resolvedWhen: 2023-04-10T19:50:20+02:00
# You can use: down, disrupted, notice
severity: notice
#affected:
section: issue
informational: true
---
A domain transfer to another registrar has been started.

Disruption of services is not expected - they all depend on other third
parties now. So as of now, this is just a heads up to **save my onion
address** in your bookmarks or something.
