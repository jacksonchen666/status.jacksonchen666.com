---
title: "Issues in DNS Resolving"
date: 2023-09-04T15:45:00Z
resolved: true
resolvedWhen: 2023-09-04T16:00:00+00:00
# You can use: down, disrupted, notice
severity: disrupted
section: issue
---
Quad9 (the DNS we use) seems to be not functional at the moment.

We do not know how this impacts other services. It may not be impacted at
all.

At the time, we will be switching the fallback DNS to Google.

# Update 2023-09-04T16:00:00+00:00

I can't figure it out, but something may be wrong with the internet
connection.

However, it seems like it's already fixed, so no more whatever.
