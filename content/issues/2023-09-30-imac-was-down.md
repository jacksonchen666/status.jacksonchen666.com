---
title: "imac Was Down"
date: 2023-09-30T04:30:00Z
resolved: true
resolvedWhen: 2023-09-30T09:10:00Z
# You can use: down, disrupted, notice
severity: down
affected:
- blackbox prober
section: issue
---
Past downtime recording
