---
title: "Soon ending Must-Staple OCSP"
date: 2025-02-10T10:30:00Z
#resolved: false
#resolvedWhen:
# You can use: down, disrupted, notice
severity: notice
affected:
- main website
- files
- jc6.xyz
- jc666.xyz
- mastodon
- matrix homeserver
- prosody
- videos
- wiki
- git (forgejo)
- status pages (laptop-server)
- calendar
- miniflux
- ntfy
- gts.hazmat.jacksonchen666.com
- another-gts.hazmat.jacksonchen666.com
section: issue
informational: true
---
# written 2025-02-10T10:30:00+00:00

[Let's Encrypt is ending OCSP support on 2025-08-06][ocspend] (and it'll be
replaced with CRLs or CRLite, see
<https://letsencrypt.org/2024/07/23/replacing-ocsp-with-crls/> for why).

[ocspend]:https://letsencrypt.org/2024/12/05/ending-ocsp/

On jacksonchen666.com, all domains have OCSP Must-Staple enabled, meaning
the webserver must provide a valid OCSP response instead of the client
querying themselves (which creates a liability for Let's Encrypt as they may
be compelled to log OCSP queries). However, because Let's Encrypt now has a
date where they'll shutdown their OCSP servers (and replace it with CRLite),
Must-Staple OCSP must soon be removed.

The first certificate to not have any Must-Staple OCSP was on the
jacksonchen666.com domain, and subdomains of that, on 2025-02-04.

The removal of Must-Staple will happen over naturally when certificates need
to be renewed. This means not all domains have removed Must-Staple until
2025-04-22.

This is only applicable to domains with HTTPS. This therefore excludes Tor,
Yggdrasil, I2P sites, and anything that doesn't use HTTP/HTTPS or any
certificates (only croc, mumble uses the same HTTPS certificates).

This should not cause any issues for web browsers, but may cause issues for
non-web browser clients, specifically around certificate revocation.
However, that problem applies to all sites using Let's Encrypt certificates,
which is a lot more than just this site.

# update 2025-02-16T23:30:00+00:00

Updating the timeline to add the following:
- 2025-05-01: Stapled OCSP will be removed from all services. This is
  separate from Must-Stapled, and happens slightly before the date where
  future certificates will lack an OCSP URL.
