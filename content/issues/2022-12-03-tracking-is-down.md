---
title: tracking is down
date: 2022-12-03T11:17:10+01:00
resolved: true
resolvedWhen: 2022-12-04T10:59:39+01:00
# You can use: down, disrupted, notice
severity: down
affected:
- analytics/tracking
section: issue
---
plausible is down aaaaaaaaaaaaaaaaaa
# timeline
## 2022-12-03T11:17:10+01:00
it is assumed this is when plausible goes down because of an automatic upgrade with watchtower (the upgrade process was actually manual, so that was a mistake)

this also sends a notification, but was ignored due to the overwhelming amount of service notifications that had been sent (for some reason).

## 2022-12-04T00:44+01:00
i [realize](https://micro.jacksonchen666.com/@jacksonchen666/109452396236439603) that analytics is down, and probably at that time, start working on it.

## some unknown time in between
i upgraded plausible, after realizing that watchtower has upgraded plausible without question.

## about 2022-12-04T01:16+01:00
i'm still working on it, but i found [this issue i had](https://github.com/plausible/analytics/discussions/2492#discussioncomment-4297188) and that [the backup script still running](https://micro.jacksonchen666.com/@jacksonchen666/109452523211046181), causing a load average of up to 30.

## 2022-12-04T01:40:20+01:00
i am writing this section of this post after i have finished the previous sections.

## 2022-12-04T01:55:31+01:00
i am impatient.
i am rebooting the server.

## 2022-12-04T10:59:39+01:00
in between 2022-12-04T10+01:00 and the time specified by the header, i tried to rollback the version of plausible.
after a while, surprise, tracking is now up.

end.

# updates
additional updates will be posted as additions to the timeline.
