---
title: "Tor Onions May Have Been Down"
date: 2024-02-24T09:20:00Z
resolved: true
resolvedWhen: 2024-02-24T12:10:00Z
# You can use: down, disrupted, notice
severity: down
affected:
- files onion
- main website over onion
- mastodon onion
- uptime status onion
- wiki onion
- status onion
section: issue
---
I'm not exactly sure, but it was probably down. It might've not been down as
well.
