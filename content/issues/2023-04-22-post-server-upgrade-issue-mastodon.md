---
title: "Post server upgrade issue: mastodon"
date: 2023-04-22T10:19:02+02:00
resolved: true
resolvedWhen: 2023-04-22T11:16:39+02:00
# You can use: down, disrupted, notice
severity: down
affected:
- mastodon
section: issue
---
