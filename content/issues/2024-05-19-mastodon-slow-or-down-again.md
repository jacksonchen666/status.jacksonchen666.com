---
title: "Mastodon Slow or Down Again"
date: 2024-05-19T11:20:00Z
resolved: true
resolvedWhen: 2024-05-19T12:20:00+00:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- mastodon
- mastodon onion
section: issue
---
# initial 2024-05-19T11:31:08+00:00

Due to a boost again, instance died.

# resolved 2024-05-19T11:55:00+00:00 - hopefully resolved

It seems like the mastodon instance is back. Marking this as resolved.

# unresolved update 2024-05-19T12:05:00+00:00

Nope it's down again lol

# resolved 2024-05-19T12:20:00+00:00

Mastodon is back up again now

# unresolved 2024-05-19T12:35:00+00:00

Mastodon is now dying again

This incident will not be marked as resolved until the Mastodon instance is
up for at least 3 hours straight.

# resolved 2024-05-19T15:19:25+00:00 - hopefully resolved

Mastodon seems to be doing mostly OK, marking this as resolved

The resolved date is pretty much the last time the instance died
