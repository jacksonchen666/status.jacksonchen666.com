---
title: matrix homeserver going down for some changes
date: 2022-07-03T13:00:00+02:00
resolved: true
resolvedWhen: 2022-07-03T15:14:26+02:00
severity: down
affected:
- matrix homeserver
section: issue
---
the matrix homeserver will be down for some changes, including enabling workers for performance reasons.

it will be down from 2022-07-03T13:00:00+02:00 and probably for the next 3 hours as a soft limit. or 9 hours as the hard limit.

# update 2022-07-03T12:43:47+02:00 - starting early
the maintenance will start early, and the matrix homeserver will be going down soon-ish.

# update 2022-07-03T14:15:01+02:00 - progress, but not complete
progress has been made after a lot of confusion and fighting with systemd (fun when the processes stop and then the source is not obvious and then it's systemd), but the media repo has not been working except for everything else.

# update 2022-07-03T14:20:18+02:00 - well that was quick
well that was pretty fast, i figured out that one of the workers did not listen to the correct thing. i guess [this guide is flawed](https://gist.github.com/tbiehn/84c43b9709374cfe1e5b0dba635b44f8).

# update 2022-07-03T15:14:26+02:00 - declaring completion
ok, i don't think there's any more to do.
that's all folks.
