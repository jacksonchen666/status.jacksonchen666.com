---
title: "Mastodon Search Not Working"
date: 2024-05-12T00:55:00Z
resolved: true
resolvedWhen: 2024-05-12T10:15:00+00:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- mastodon
- mastodon onion
section: issue
---
# created 2024-05-12T00:55:00+00:00

After upgrading Mastodon, the full-text search functionality is not working.

Due to, uh, things, the OpenSearch backend had to be stopped so that
Mastodon still kinda works.

This only affects local users.

# update 2024-05-12T01:30:00+00:00

We have tried to do a hot fix, but somehow our OpenSearch install became
non-functional. We'll fix OpenSearch later due to the current time being
late.

# resolved 2024-05-12T10:15:00+00:00

We just threw our OpenSearch installation out of the window and created
another one, which seems to have fixed the issue with OpenSearch being
broken.

Now Mastodon's full text search works again.
