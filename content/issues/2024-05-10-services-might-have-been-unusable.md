---
title: "Services Might Have Been Unusable"
date: 2024-05-10T20:50:00Z
resolved: true
resolvedWhen: 2024-05-11T16:20:00+00:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- reinfo wiki
- wiki
- uptime status
- miniflux
- ntfy
section: issue
---
# report 2024-05-11T16:20:00+00:00
At the start date of the incident (not reported in the header right above),
HTTP/3 was turned on for all services.

This has however caused a bunch of other random services to break for
reasons still unknown when used.

HTTP/3 has been turned off for services that are broken with it enabled.

The start time is a rough estimate, as the monitoring did not catch this
(due to it not using HTTP/3 for checking).
