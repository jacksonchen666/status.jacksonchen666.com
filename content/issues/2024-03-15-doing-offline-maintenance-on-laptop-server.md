---
title: "Doing Offline Maintenance on laptop-server"
date: 2024-03-15T13:40:00Z
resolved: true
resolvedWhen: 2024-03-15T14:05:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
- files
- i2p site
- jc6.xyz
- jc666.xyz
- main gemini capsule
- main website
- mastodon
- matrix homeserver
- prosody
- reinfo wiki
- status pages (laptop-server)
- uptime status
- videos
- wiki
- files onion
- main website over onion
- mastodon onion
- reinfo wiki onion
- status onion
- uptime status onion
- wiki onion
- main website over yggdrasil
section: issue
---
laptop-server will be shutdown to do some maintenance on it.

This maintenance has been started.

# update 2024-03-15T14:05:00+00:00 - maintenance over
This maintenance is now over.
