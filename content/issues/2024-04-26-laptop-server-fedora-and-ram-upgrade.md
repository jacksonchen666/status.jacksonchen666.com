---
title: "laptop-server Fedora and RAM Upgrade"
date: 2024-04-26T14:00:00Z
resolved: true
resolvedWhen: 2024-04-26T15:45:00+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
#server
- main website
- main gemini capsule
- files
- jc6.xyz
- jc666.xyz
- mastodon
- matrix homeserver
- prosody
- reinfo wiki
- videos
- wiki
- status pages (laptop-server)
- uptime status
- i2p site
- files onion
- main website over onion
- mastodon onion
- reinfo wiki onion
- status onion
- uptime status onion
- wiki onion
- main website over yggdrasil
- calendar
- miniflux
- ntfy
- gts.hazmat.jacksonchen666.com
- another-gts.hazmat.jacksonchen666.com
- mumble server
- croc
section: issue
---
# announcement - 2024-04-22T07:47:28+00:00

NOTE: [This event has been rescheduled](#reschedule) to account for time
spent doing backups, which means time being down.

Hi! On 2024-04-26 (Friday of this week), I'll be taking down laptop-server
for maintenance of 2 things:
- Fedora upgrade
- RAM upgrade

According to [the Fedora 40 schedule][f40sched], the "Current Final Target
date" is 2024-04-23 (Tuesday), and [the Fedora 40 Go/No-Go meeting concluded
with "GO"][f40finalgonogo], so Fedora 40 will be released this Tuesday.

[f40sched]:https://fedorapeople.org/groups/schedule/f-40/f-40-key-tasks.html

[f40finalgonogo]:https://lists.fedoraproject.org/archives/list/logistics@lists.fedoraproject.org/thread/MZMGHPSYWTCVVM6LV67EBBOS7X7MS2P5/

laptop-server will be prepared with a backup before the upgrade, which will
be met with extreme performance hits while a backup happens. The backup will
probably last for at least a couple of hours, and **services may be shut
off** to prevent data loss in case of disaster.

laptop-server will also be receiving a RAM upgrade, which means downtime.
This page will include both the Fedora upgrade and the RAM upgrade, and
other uptime issues will be created if any issues arises after the Fedora
upgrade.

This maintenance event should take 2 or 3 hours to do (with a hard limit of
6 hours), and will probably happen at these times:
- ~~2024-04-26T16:00:00Z~~
- ~~2024-04-26T17:00:00Z~~

NOTE: [This event has been rescheduled](#reschedule) to account for time
spent doing backups, which means time being down.

# update 2024-04-25T20:26:40+00:00 - rescheduling/additional scheduling {id=reschedule}

I uh, need to change the times and stuff. And include more information.

The previous can probably ignored in its entirety. Maybe.

So, here's the timeline plan:
- 2024-04-26T14:00:00Z: ~~Shut off any services that can write data, and start
  creating a backup. This part may take 1 hour as previous backups suggest,
  however since services are intentionally down (to reduce potential data
  loss), the duration may not be exact (and the actual time to do a full
  backup is 1.5 hours). This means the following will be down:~~
    - ~~Mastodon~~
    - ~~Matrix Homeserver~~
    - ~~Prosody~~
    - ~~Videos/PeerTube~~
    - ~~Wiki (complete shutdown/unavailability)~~
- 2024-04-26T15:00:00Z: Start offline software upgrade. The server will not
  serve anything at all during the period. This will take roughly 20
  minutes, as previous upgrades suggest.
- 2024-04-26T15:20:00Z: Start doing hardware (RAM) upgrade. laptop-server
  will be taken offline again, and this will take an unknown amount of time,
  although we hope that it will take about 30 minutes to complete.
- After the hardware upgrade, any other issues post-upgrade will be then
  resolved.

**Specified times are not strict and may vary by ±30 minutes or even more.**
The absolute maximum time will be 12 hours. If this takes more than 12
hours, something has probably gone very wrong.

Otherwise, it's pretty much the same thing.

# update 2024-04-26T11:59:14+00:00 - services that will be stopped

I've decided to instead stop all services instead of partial shutdown during
the phrase of backups.

All services will be shutdown on 2024-04-26T14:00:00Z.

# starting maintenance 2024-04-26T14:00:00+00:00

All services for hosting will be stopped, and a backup will be started.

# update 2024-04-26T14:50:00+00:00 - moving onto upgrade

The backup has been completed and the reboot for the upgrade has been
started.

# update 2024-04-26T15:30:00+00:00 - moving onto RAM upgrade

The software upgrade has been completed, and we are moving onto the hardware
upgrade.

# update 2024-04-26T15:35:00+00:00 - RAM upgrade completed

The RAM upgrade has been completed, and now we are moving onto checking if
things went well, and finally fully starting the server after it's done.

# update 2024-04-26T15:40:00+00:00 - RAM upgrade completed

The RAM upgrade has been completed. All services will soon be started, and
this page will be marked as "resolved".

Any issues with any service post-upgrade will have its own dedicated issue
page.

# complete 2024-04-26T15:45:00+00:00

This issue will be marked as resolved, as it has been finished.

Other issues are happening, and those will be recorded.
