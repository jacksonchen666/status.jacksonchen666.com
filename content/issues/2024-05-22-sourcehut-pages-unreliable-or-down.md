---
title: "Sourcehut Pages Unreliable or Down"
date: 2024-05-22T08:00:00Z
resolved: true
resolvedWhen: 2024-05-22T13:50:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
- status pages
- main website backup
- main gemini capsule backup
section: issue
---
# initial 2024-05-22T11:05:00+00:00
There is quite sporadic uptime on sourcehut pages right now, and it's not
clear why. But it seems to be happening.

# resolved 2024-05-22T13:50:00+00:00
The issue is over now.
