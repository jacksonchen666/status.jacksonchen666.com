---
title: server unreachable
date: 2023-05-30T08:25:00+02:00
resolved: true
resolvedWhen: 2023-05-30T18:00:00+02:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
- matrix chat client
- schildichat
- main website
- mastodon
- jc666.xyz
- files
- uptime kuma
- videos
- prometheus
- alertmanager
- postgres database (internal)
- prosody
- main website over onion
- files onion
- status onion
- i2p site
section: issue
---
power outage, probably
