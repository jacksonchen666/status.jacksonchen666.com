---
title: "Peertube Update"
date: 2023-11-28T12:05:00Z
resolved: true
resolvedWhen: 2023-11-28T12:50:00+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
- videos
section: issue
---
<https://github.com/Chocobozzz/PeerTube/releases/tag/v6.0.0>

Due to a lot of changes for the sysadmin to deal with, we are going to make
some backups then apply them while our PeerTube instance is offline.

Will be starting at indicated time without notice.

# update 2023-11-28T12:47:06+00:00 - done

The upgrade is complete.

Storyboards (thumbnail on progress bar hover) will be processed soon, and
videos that need replacing will be replaced.
