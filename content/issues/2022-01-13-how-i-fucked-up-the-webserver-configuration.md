---
title: how i fucked up the webserver configuration
migrated: true
date: 2022-01-13T18:40:27+01:00
resolved: true
affected:
- new blog
section: issue
informational: true
resolvedWhen: 2022-01-13T18:40:27+01:00
severity: down
---

hi and welcome to the ~~exact place~~ where i accidentally dropped the configuration and forgot about it for a few days (actually i have no idea when i screwed up). it was showing the default page for a webserver without configuration (only on the blogs because that's where i screwed up), and i spotted the issue while [working on search functionality](https://gitlab.com/JacksonChen666/JacksonChen666.gitlab.io/-/merge_requests/15).

now, you're surprised (or not) that i managed to delete the configuration and then forget about it. what i usually do is test, and forget about it. i'm not sure if i tested that time with when i moved the configuration to a snippet to make the configuration less duplicate and bloated with the same thing, because unfortunately, i forgot to quite literally `include` the snippet of configuration.

another mistake is not having a user called "webmaster" on my email domains, so that means i may or may not have missed emails. i've gotten that done, so someone can still email me if something breaks and it says to send an email to that address.

and finally, i have not taken steps to prevent this from happening again in the future. because i can't and because i literally forgot to `include` the configuration in the first place. the problem is, what are even the steps to take? there kinda isn't any!
i have taken steps to be available to contact when something goes wrong (though by available i mean having an alias to webmaster).

anyways, i hope you enjoy the content here again. sorry for the downtime on blogs.
