---
title: "Server Completely Unreachable"
date: 2024-01-17T16:10:00Z
resolved: true
resolvedWhen: 2024-01-17T16:50:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
#server
- alertmanager
- files
- i2p site
- jc6.xyz
- jc666.xyz
- main gemini capsule
- main website
- main website over yggdrasil
- mastodon
- matrix homeserver
- prometheus
- prosody
- reinfo wiki
- uptime status
- videos
- files onion
- main website over onion
- mastodon onion
- reinfo wiki onion
- uptime status onion
- status onion
#imac
- blackbox prober
section: issue
---
No idea what's happening, but I cannot connect to neither laptop-server or
imac, nor any of its alternative access points (Tor onions, yggdrasil)

# update 2024-01-17T16:41:55+00:00 - it might be back up, but badly

I've noticed something happen, it's probably back up again.

BRB going to fix things.

# update 2024-01-17T16:50:00+00:00 - things should be back

The server is now back up.
