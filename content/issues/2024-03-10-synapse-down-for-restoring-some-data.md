---
title: "Synapse Down for Restoring Some Data"
date: 2024-03-10T16:45:00Z
resolved: true
resolvedWhen: 2024-03-10T22:10:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
section: issue
---
# update 2024-03-10T22:10:00+00:00 - some local media restored
Some local media was accidentally deleted, and now has been restored.

No data loss should've occurred.
