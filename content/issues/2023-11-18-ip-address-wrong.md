---
title: "IP Address Wrong"
date: 2023-11-18T11:35:00Z
resolved: true
resolvedWhen: 2023-11-18T13:15:00Z
# You can use: down, disrupted, notice
severity: disrupted
affected:
- alertmanager
- files
- jc6.xyz
- jc666.xyz
- main gemini capsule
- main website
- mastodon
- matrix homeserver
- prometheus
- prosody
- reinfo wiki
- uptime status
- videos
section: issue
---
Our IPs changed, but ddclient was not running due to issues experienced a
day and 2 ago.

This post is just a record of that incident.
