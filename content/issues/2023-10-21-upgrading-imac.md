---
title: "Upgrading imac"
date: 2023-10-21T17:50:00Z
resolved: true
resolvedWhen: 2023-10-21T18:15:00+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
- blackbox prober
section: issue
---
