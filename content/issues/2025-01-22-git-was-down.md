---
title: "Git Was Down"
date: 2025-01-22T08:25:00Z
resolved: true
resolvedWhen: 2025-01-22T09:45:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
- git (forgejo)
section: issue
---
# resolved 2025-01-22T09:45:00+00:00

Due to an issue possibly in the third-party package used for forgejo,
forgejo broke. This was fixed.

The issue seems to lie with user management and hook running order. The
removal hooks were run last after installation hooks on upgrade or
reinstall, and also ran unconditionally. Even then, the UID and GID had
changed because the user was removed, which breaks permissions. It also
stopped the service from running, which is why this issue happened.

An issue will be made with the third-party package maintainer.
