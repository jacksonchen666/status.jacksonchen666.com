---
title: "Upgrading imac"
date: 2023-10-07T19:35:00Z
resolved: true
resolvedWhen: 2023-10-07T19:45:00+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
- blackbox prober
section: issue
---
