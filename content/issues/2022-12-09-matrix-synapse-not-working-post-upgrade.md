---
title: matrix synapse not working post upgrade
date: 2022-12-09T19:17:09+01:00
resolved: true
resolvedWhen: 2022-12-09T19:29:15+01:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
section: issue
---
