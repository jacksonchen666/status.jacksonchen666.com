---
title: "Network Down for Maintenance"
date: 2023-07-15T13:30:00Z
resolved: true
resolvedWhen: 2023-07-15T13:40:00+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
- matrix homeserver
- matrix chat client
- schildichat
- main website
- main website (srht.site)
- status pages (srht.site)
- mastodon
- jc666.xyz
- files
- uptime monitor
- videos
- prometheus
- alertmanager
#- postgres database (internal)
- prosody
- main website over onion
- files onion
- status onion
- i2p site
section: issue
---
For a bit
