---
title: tracking/analytics will be shutdown
date: 2022-12-25T12:19:13+01:00
# You can use: down, disrupted, notice
severity: down
affected:
- analytics/tracking
section: issue
informational: true
---
plausible (analytics/tracking) will now be shutdown, following the [removal of the analytics](https://jacksonchen666.com/posts/2022-12-16/00-29-00/).

this is because it is wasting precious resources (RAM, it is currently using 1718M of swap)

the data will not be available for browsing... yet (tm)
