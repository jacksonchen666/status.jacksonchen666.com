---
title: "Some Services May Show Fallback Page Sometimes"
#date: 2024-05-10T20:50:00Z
date: 2024-06-13T14:15:40+00:00
resolved: true
resolvedWhen: 2024-06-13T14:15:40+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
##server
#- main website
#- main gemini capsule
#- files
#- jc6.xyz
#- jc666.xyz
- mastodon
- matrix homeserver
#- prosody
- reinfo wiki
- videos
- wiki
- status pages (laptop-server)
- uptime status
- calendar
- miniflux
- ntfy
- gts.hazmat.jacksonchen666.com
- another-gts.hazmat.jacksonchen666.com
#- mumble server
#- croc
##sourcehut pages
#- status pages
#- main website backup
#- main gemini capsule backup
section: issue
informational: true
---
# postmortem report 2024-06-13T14:15:40+00:00

We found out why sometimes services show the fallback page similar to the
one on <https://fallback.jacksonchen666.com>: HTTP/3

That lacks context. So here's the context:
- nginx reverse proxying with HTTP/3 works badly for almost all services, so
  it's not enabled for those services.
- However, it is enabled for a couple of services, including the main
  website at <https://jacksonchen666.com> and a couple of other things.
- We use the `reuseport` option for listening with the QUIC protocol
  (similar to
  <https://stackoverflow.com/questions/76348128/enabling-quic-http-3-on-multiple-domains-with-nginx-1-25>)
- If clients use HTTP/3 on a service which doesn't support HTTP/3 and has it
  disabled, nginx will just return whatever applicable, which is the
  fallback page.

We have made a quick hotfix which should prevent this problem from
happening, however the hotfix is just disabling HTTP/3 entirely, so there
will no longer be HTTP/3 until fixed (which may be complicated).

Some info about this report
- This report is marked as informational because when this happens also
  depends on many conditions (first you must visit an http/3 enabled
  service, then a http/3 disabled service for the issue to happen)
- Affected services are just things which don't support http/3
- 2024-05-10T20:50:00Z is when HTTP/3 was turned on, or where HTTP/3
  could've caused the mentioned problems.

# update 2024-06-13T16:05:00+00:00

It's unlikely that HTTP/3 support will be coming back as we couldn't get
things working on a secondary port.

All HTTP/3 related stuff will be torn down now.
