---
title: "Backups Testing"
date: 2024-12-18T16:05:00Z
resolved: true
resolvedWhen: 2024-12-19T00:00:00Z
# You can use: down, disrupted, notice
severity: notice
#affected:
section: issue
---
# initial 2024-12-18T16:05:00+00:00

This is a post announcing that laptop-server will be running a backup, which
will take an hour. In that hour, performance across roughly everything will
be absolutely abysmal.

The purpose of this is to diagnose issues with the backups being slow to do
(30 minutes for borgbackup, much slower than [the comparison on the
borgbackup docs][docsperformance]).

This may happen multiple times, and this will be marked resolved after all
the testing is complete.

No services should be affected by the severe performance degredation. imac
and sourcehut pages category items are also unaffected.

[docsperformance]:https://borgbackup.readthedocs.io/en/stable/faq.html#what-s-the-expected-backup-performance

# update 2024-12-18T20:10:00+00:00

Another backup has been started. Some changes has been made, but it is
unknown how it'll affect the performance of the backup.

# finally marking as resolved 2024-12-20T16:10:00+00:00

This status was left unresolved by accident. It has been finished already.

Not much changes happened. Backups times will probably still suck.
