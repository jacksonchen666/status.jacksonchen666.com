---
title: synapse down
date: 2023-01-02T22:35:14+01:00
resolved: true
resolvedWhen: 2023-01-02T23:00:13+01:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
section: issue
---
