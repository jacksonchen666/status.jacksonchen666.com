---
title: disrupted federation with matrix hs
date: 2022-07-03T15:00:00+02:00
resolved: true
resolvedWhen: 2022-07-04T02:50:00+02:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- matrix homeserver
section: issue
---
so... setting up workers went wrong when i went on my own, because i included some things the generic worker did not handle.
as such, things just failed to work and was completely silent.

this issue has since been fixed
