---
title: "Servers Down"
date: 2024-10-28T21:15:00Z
resolved: true
resolvedWhen: 2024-10-28T22:45:00Z
# You can use: down, disrupted, notice
severity: down
affected:
#server
- main website
- main gemini capsule
- files
- jc6.xyz
- jc666.xyz
- mastodon
- matrix homeserver
- prosody
- reinfo wiki
- videos
- wiki
- status pages (laptop-server)
- uptime status
- i2p site
- files onion
- main website over onion
- mastodon onion
- reinfo wiki onion
- status onion
- uptime status onion
- wiki onion
- main website over yggdrasil
- calendar
- miniflux
- ntfy
- gts.hazmat.jacksonchen666.com
- another-gts.hazmat.jacksonchen666.com
- mumble server
- croc
#imac
- blackbox prober
##sourcehut pages
#- status pages
#- main website backup
#- main gemini capsule backup
section: issue
---
# initial report 2024-10-28T21:15:00Z

For whatever reason, laptop-server and imac has become unreachable.

I am currently unable to investigate or find anything out about this, other
than it's a possible IP change. However, this is taking way too long now.

# resolved 2024-10-28T22:45:00Z

This issue kind of resolved itself.

Basically: IP change. Something happened with unbound on laptop-server,
which made it unable to reach the domain name to send IP changes to, so it
couldn't update its IP. No idea why it got into this state.

I attempted to manually change the IP address (to be able to access VPN and
therefore, laptop-server), but then the IP changed again. Then laptop-server
was able to resolve the DNS to change the IP.
