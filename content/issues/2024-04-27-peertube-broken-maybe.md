---
title: "PeerTube Broken Maybe"
date: 2024-04-27T10:00:00Z
resolved: true
resolvedWhen: 2024-04-29T11:55:00Z
# You can use: down, disrupted, notice
severity: down
affected:
- videos
section: issue
---
# incident recording 2024-04-27T11:10:00+00:00

# resolved 2024-04-29T17:15:00+00:00
I feel as though there should be some explanation.

So PeerTube started querying for stuff that made postgresql processes eat up
all the CPU and cause server performance issues. We tried restarting but
PeerTube would just end up doing the things again.

We kept trying over multiple days but ended up having to stop PeerTube
multiple times because it was causing extreme load to the servers.

Today, we tried running on a single database connection. That somehow made
PeerTube process all the things after letting it run for a while.

That time of finishing the processing seems to have happened at around
2024-04-29T11:55:00Z. So that's the resolved time.
