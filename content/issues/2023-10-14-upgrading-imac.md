---
title: "Upgrading imac"
date: 2023-10-14T18:20:00Z
resolved: true
resolvedWhen: 2023-10-14T18:35:00+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
- blackbox prober
section: issue
---
