---
title: "imac RAM Upgrade"
date: 2024-05-03T14:55:00Z
resolved: true
resolvedWhen: 2024-05-03T15:45:00+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
- uptime status
- uptime status onion
section: issue
---
# initial 2024-05-03T07:45:00+00:00
We'll be doing a RAM upgrade on imac.

This will only affect the uptime monitoring on
<https://uptime.jacksonchen666.com>, which will have inaccurate stats while
imac is down.

imac will be shutdown on 2024-05-03T14:50:00Z, and will probably be down for
around 30 minutes (2 hours at absolute maximum).

# starting 2024-05-03T14:58:43+00:00
imac will be shutdown in preparations for the RAM upgrade. Uptime status
(not the status page) will be inaccurate, and services may be down without
the sysadmin noticing.

# finished 2024-05-03T15:45:00+00:00
The imac RAM upgrade has been finished. Services should be back to normal.
