---
title: troubleshooting server
date: 2023-01-13T18:35:33+01:00
resolved: true
resolvedWhen: 2023-01-13T19:23:00+01:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
- matrix chat client
- mastodon
- files
- uptime kuma
- videos
- prometheus
- alertmanager
- grafana
- postgresql database (internal)
- main website over onion
- files onion
- status onion
section: issue
---
so i renamed the volume group on the server, rebooted, and as somewhat
unexpected, it took too long to reboot.

i had forced restart the server, and i am now troubleshooting the issue.
