---
title: "Some internal changes and stuff"
date: 2023-07-10T13:30:00+00:00
resolved: true
resolvedWhen: 2023-07-10T14:15:00+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
- matrix homeserver
- matrix chat client
- schildichat
- main website
- mastodon
- jc666.xyz
- files
- uptime monitor
- videos
- prometheus
- alertmanager
- postgres database (internal)
- prosody
- main website over onion
- files onion
- status onion
- i2p site
section: issue
---
Doing some messing around with internal network, things may go down while
things are being fixed.
