---
title: "Sourcehut Down Again"
date: 2024-01-14T22:00:00Z
resolved: true
resolvedWhen: 2024-01-14T22:30:00Z
# You can use: down, disrupted, notice
severity: down
affected:
- main gemini capsule backup
- main website backup
- status pages
section: issue
---
