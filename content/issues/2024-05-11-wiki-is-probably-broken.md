---
title: "Wiki Is Probably Broken"
date: 2024-05-11T09:45:00Z
resolved: true
resolvedWhen: 2024-05-11T16:05:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
- wiki
section: issue
---
# report 2024-05-11T15:15:00+00:00
Wiki may not be working at all currently.

# update 2024-05-11T15:20:00+00:00 - likely cause and start time determined
The severity level has been updated to "down" and the start time has been
adjusted, because of an automatic upgrade that upgraded MediaWiki without
warning to 1.41.1. And the wiki does not work in general.

# update 2024-05-11T16:00:00+00:00 - partially fixed
We've mostly fixed MediaWiki by upgrading extensions as well after the
upgrade.

We're not done yet, as the main page is broken right now.

# resolved 2024-05-11T16:05:00+00:00
The immediately obvious issues with the MediaWiki has been resolved, so this
incident is marked as resolved.
