---
title: planned major server software upgrade with downtime
date: 2022-05-10T18:00:00+02:00
resolved: true
section: issue
resolvedWhen: 2022-05-10T18:38:00+02:00
severity: down
disrupted:
- all
---
oh hey hi, i kinda [recently upgraded the server (hardware)](/maintenance/2022-04-04/17-50-44/) <!-- Warning: Breakable link! --> and wasn't really aware that [fedora is having another release soon](https://web.archive.org/web/20220414142141/https://alt.fedoraproject.org/prerelease/index.html) [according to their schedule][schedule] (see "Current Final Target Date" or actually, "Final Target date #1" which is what Fedora users are supposed to plan on) so time for more planning!

with the last upgrade from fedora 34 to 35, yeah the system did have to be down for some time.

using 2022-04-26 and 2022-04-19 as references of the potential release dates (and the beta being released on not the early target), here's the following dates and times on when the upgrade should happen (that is, if i can upgrade to fedora 36):
- ~~2022-04-19T12:00:00+02:00~~ (less likely, but potentially)
- ~~2022-04-20T12:00:00+02:00~~ (less likely, but potentially)
- ~~2022-04-26T18:00:00+02:00~~ (**most likely**)
- ~~2022-04-27T18:00:00+02:00~~ (*very* unlikely)
- ~~2022-05-03T18:00:00+02:00~~ (**next most likely**)
- ~~2022-05-04T18:00:00+02:00~~ (*very* unlikely)
- 2022-05-10T18:00:00+02:00 (**next *next* most likely**)
- ~~2022-05-11T18:00:00+02:00~~ (**unlikely**)

only 1 of the time will be picked (or announced as an update if i don't do it at any of the mentioned time).

but even with all the set times, i might have to be a bit late because i arrived too late to the server location. but otherwise, an update will be given when the upgrade starts.

otherwise, the time it should take for the upgrade (basing off of the previous release upgrade which took 26 minutes with the hard drive and was recorded by `dnf` itself) is, reasonably, around 1 hour.
the soft time limit is 3 hours for this downtime, unless things went wrong in any part of the process.

# update 2022-04-26T16:29:59+02:00 - affected services
all subdomain services (like blog) except microblogging will be down while the update is going on.

# update 2022-04-26T22:16:49+02:00 - fedora 36 not released due to release blocking bug
yeah, someone discovered a [bug](https://bugzilla.redhat.com/show_bug.cgi?id=2071228) that prevented the release (and [here's any release blocking bugs, see what it depends on][blocker bugs]).
hopefully it is at most next week, or i might have to cross off everything then wait until it can be done.

otherwise, no downtime or upgrade unfortunately.

# update 2022-04-28T22:21:33+02:00 - fedora 36 delayed again
the [schedule] just added another target date (#3), and [5 bugs are currently accepted for blocking release (5 accepted blockers)](https://web.archive.org/web/20220428203932/https://qa.fedoraproject.org/blockerbugs/milestone/36/final/buglist).

i don't even know when to expect a release now, so just check back every monday for an update of plans if the release is going to really be happening.
rss probably won't work for this, since updates are changes and not new posts.

# update 2022-05-07T12:30:57+02:00 - a possibly good set date, finally
no blockers bugs exist ([source][blocker bugs]), the [schedule] hasn't changed, now expect that the upgrade will actually happen at the specified time (assuming there's not going to be blockers again).

that's about it

# update 2022-05-10T16:53:10+02:00 - planned time is now confirmed
fedora 36 has been released, and you should probably upgrade too.

with that said, the said date of 2022-05-10T18:00:00+02:00 will be the start of the time where the server would go down for the upgrading process.

- new soft time limit: 2 hours
- hard time limit (overrides previous ones): 4 hours

# update 2022-05-10T18:38:00+02:00 - upgrade complete
upgrade is complete. that's all. this post is over.

[schedule]: https://fedorapeople.org/groups/schedule/f-36/f-36-key-tasks.html
[blocker bugs]: https://bugzilla.redhat.com/show_bug.cgi?id=1953785
