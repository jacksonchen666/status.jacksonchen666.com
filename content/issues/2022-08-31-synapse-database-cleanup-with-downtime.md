---
title: synapse database cleanup with downtime
date: 2022-08-31T16:24:43+02:00
resolved: true
resolvedWhen: 2022-08-31T18:43:31+02:00
# You can use: down, disrupted, notice
severity: notice
affected:
- matrix homeserver
section: issue
---
hello. today, the synapse is being shutdown temporarily for a database cleanup.
this may take around an hour or so. hard limit is 3 hours from the date.
the matrix homeserver will be shutdown when this post is live.

the following will be done:
- all rooms without any users joined will be deleted from the database
- all rooms will have history truncated to 14 days excluding local events
- all remote media older than 14 days will be removed
- states will be compressed
- a full reindex and vacuum will be done on the database (synapse will likely be unable to operate at the time, so it is shutdown anyways)

# update 2022-08-31T17:45:52+02:00 - maintenance finished
maintenance has been finished. that's all.

# update 2022-08-31T18:43:31+02:00 - oops, it was still down
sorry, but synapse was down because i forgot to start it back up.
it is now up.
