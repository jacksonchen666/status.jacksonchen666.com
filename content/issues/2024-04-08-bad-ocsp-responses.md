---
title: "Bad OCSP Responses"
date: 2024-04-08T09:40:00Z
resolved: true
resolvedWhen: 2024-04-08T09:45:00Z
# You can use: down, disrupted, notice
severity: disrupted
affected:
- files
- main website
- mastodon
- matrix homeserver
- prosody
- status pages (laptop-server)
- uptime status
- videos
- wiki
section: issue
---
Certificates for jacksonchen666.com and \*.jacksonchen666.com was renewed
(at the start of the incident recorded in this post), however the cached
OCSP was forgotten to be updated.

This is a past incident, recorded at 2024-04-08T10:40:00+00:00. It has
already been resolved.
