---
title: long onion downtime
date: 2022-10-19T03:51:39+02:00
resolved: true
resolvedWhen: 2022-10-19T13:23:29+02:00
# You can use: down, disrupted, notice
severity: down
affected:
- main website over onion
- files onion
section: issue
---
from uptime kuma
