---
title: "Peertube Upgrade Fail"
date: 2023-09-04T11:15:00Z
resolved: true
resolvedWhen: 2023-09-04T11:40:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
- videos
section: issue
---
Our ffmpeg install is broken, for some reason.

# Update 2023-09-04T11:40:00+00:00

Downgraded ffmpeg, seems to work.

Downgraded to `6.0-6.fc38` from `6.0-11.fc38`.
