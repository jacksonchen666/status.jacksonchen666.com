---
title: "SourceHut Was Down"
date: 2024-06-06T03:45:00Z
resolved: true
resolvedWhen: 2024-06-06T06:20:00Z
# You can use: down, disrupted, notice
severity: down
affected:
- status pages
- main website backup
- main gemini capsule backup
section: issue
---
# incident reporting 2024-06-06T06:50:00+00:00

That's it. It's already over.
