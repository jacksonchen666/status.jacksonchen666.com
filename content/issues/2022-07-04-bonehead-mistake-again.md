---
title: bonehead mistake again
date: 2022-07-04T11:00:00+02:00
resolved: true
resolvedWhen: 2022-07-04T14:02:18+02:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- matrix homeserver
section: issue
---
i made another bonehead mistake with federation: making the federation sender receive requests. it does not handle any requests. it only sends.

that somehow happened. probably because it was 3 am when i tried setting up federation senders.

as such, i have disassembled the workers setup and reverted back to synapse without workers and will get a properly working infrastructure of synapse with workers redesigned.
