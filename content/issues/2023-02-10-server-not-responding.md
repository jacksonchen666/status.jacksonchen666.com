---
title: server not responding
date: 2023-02-10T16:08:41+01:00
resolved: true
resolvedWhen: 2023-02-10T18:15:31+01:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- matrix homeserver
- matrix chat client
- mastodon
- files
- uptime kuma
- videos
- prometheus
- alertmanager
- grafana
- main website over onion
- files onion
- status onion
section: issue
---
symptoms:
- server does not respond to requests
- not responding to pings
- onions are not responding (from tor browser: "0xF2 — Introduction failed,
  which means that the descriptor was found but the service is no longer
  connected to the introduction point. It is likely that the service has
  changed its descriptor or that it is not running.")

conclusion: internet outage

# update 2023-02-10T18:15:31+01:00 - problem found
the problem was found: the router was not powered on for some reason.

that has been fixed

# update 2023-02-10T18:21:13+01:00 - starting date and time changed
originally noticed issue at 2023-02-10T17:04:19+01:00, now changed to
2023-02-10T16:08:41+01:00 for notice of issue

# 2023-02-10T18:47:59+01:00 - post-mortem timeline
- 2023-02-10T16:08:36+01:00: the wireless network interface goes
  disconnected, implying the wireless network died.
- 2023-02-10T16:08:56+01:00: first report from prometheus node_exporter that
  all ~~slaves~~ connections for the bond interface is down, meaning the
  router died.
- 2023-02-10T17:04:19+01:00: time of notice on server issue
- 2023-02-10T18:15:31+01:00: the issue is marked as resolved

conclusion: the router was off... for no explainable reason:
- it was off
- power is working
- unplugging the power and plugging it back in did not work
- moving the power plugs around made it work, and the socket was working
