---
title: mastodon maintenance
date: 2023-06-12T19:00:00+02:00
resolved: true
resolvedWhen: 2023-06-12T19:30:00+02:00
# You can use: down, disrupted, notice
severity: notice
affected:
- mastodon
section: issue
---
doing some stuff

<https://github.com/glitch-soc/mastodon/issues/2247#issuecomment-1587541181>
