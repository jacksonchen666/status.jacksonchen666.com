---
title: server going down for some changes
date: 2022-10-31T13:00:00+01:00
resolved: true
resolvedWhen: 2022-10-31T14:27:47+01:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
- matrix chat client
- mastodon
- files
- rsync
- analytics/tracking
- uptime kuma
- videos
- postgresql database
- main website over onion
- files onion
section: issue
---
hello

today i decided to resize down the rootfs on the server, to make some more space for future stuff when i need a lot of space.

because this is a ext4 resize resizing down, the operation can only done when the partition is unmounted.
because the rootfs has to be unmounted, it is not possible to run the server while resizing the filesystem.
as such, the server will be shutdown temporally to conduct the changes.

the operation will start on the set date, and should finish within ~2 hours which will be the soft limit time.
the hard limit time is 4 hours. any more time with the server down may mean something has gone wrong (or it is not finished yet).

that's all. see you later

# update 2022-10-31T13:21:41+01:00 - going down
backup has been finished, now going down for resize.

# update 2022-10-31T14:27:47+01:00 - finished
the resize has finished with `e2fsck` reporting things that could be fixed and optimized.
the server will be returning to normal if nothing has went wrong with this operation.
