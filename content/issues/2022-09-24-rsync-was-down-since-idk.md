---
title: rsync was down since... idk
date: 2022-09-24T12:15:12+02:00
resolved: true
resolvedWhen: 2022-09-24T12:15:12+02:00
# You can use: down, disrupted, notice
severity: down
affected:
- rsync
section: issue
informational: true
---
this issue was created because it was recently noticed that rsync was not available.
however, the duration of how long it was unavailable is unknown.
if you have any information on my rsync daemon not being available and when, please send some information to my contact on my main website.
