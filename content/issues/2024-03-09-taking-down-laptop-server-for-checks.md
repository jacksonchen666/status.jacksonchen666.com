---
title: "Taking Down laptop-server for Checks"
date: 2024-03-09T11:15:00Z
resolved: true
resolvedWhen: 2024-03-09T20:05:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
- files
- i2p site
- jc6.xyz
- jc666.xyz
- main gemini capsule
- main website
- mastodon
- matrix homeserver
- prosody
- reinfo wiki
- status pages (laptop-server)
- uptime status
- videos
- wiki
- files onion
- main website over onion
- mastodon onion
- reinfo wiki onion
- status onion
- uptime status onion
- wiki onion
- main website over yggdrasil
section: issue
---
Something weird is happening with the I/O performance of laptop-server when
taking a backup.

Because we weren't sure what was happening, we are conducting checks to make
sure everything related to the filesystem and everything on the disk was
fine (smart tests and data).

However, we've now reached the point where we need to conduct further
checks that disrupts the operation of the website (fsck of the filesystems).

We also will take a backup of the server as a precaution for complete data
loss.

# update 2024-03-09T11:52:49+00:00
We are currently performing a system backup. However, due to things
happening, the actual filesystem checking will be taking place much later,
with the server still being down before the filesystem checking.

This maintenance has only a hard limit time of 48 hours. Anything longer
than that and something is potentially very very wrong.

# update 2024-03-09T20:05:00+00:00
Checks are complete and seems to be fine.
