---
title: server unreachable
date: 2022-12-17T19:07:00+01:00
resolved: true
resolvedWhen: 2022-12-17T22:49:18+01:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- matrix homeserver
- matrix chat client
- mastodon
- files
- analytics/tracking
- uptime kuma
- videos
- main website over onion
- files onion
- status onion
section: issue
---
