---
title: "Mastodon Intentionally Shut Off"
date: 2024-02-15T00:50:00Z
resolved: true
resolvedWhen: 2024-02-15T06:10:00Z
# You can use: down, disrupted, notice
severity: down
affected:
- mastodon
- mastodon onion
section: issue
---
