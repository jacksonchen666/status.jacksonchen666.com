---
title: "PeerTube Maintenance"
date: 2024-07-15T02:10:00Z
resolved: true
resolvedWhen: 2024-07-15T03:25:00Z
# You can use: down, disrupted, notice
severity: notice
affected:
- videos
section: issue
---
# maintenance 2024-07-15T02:10:00+00:00

We are doing a bit of clean up with PeerTube files. This requires PeerTube
to be down however, so it is down for now.

# update 2024-07-15T02:35:00+00:00

We don't have an estimation of how long it will take, other than roughly
"quite some time".

PeerTube has to delete over 275k files, which is a lot of files. So this
maintenance will take quite a while.

# complete 2024-07-15T03:25:00+00:00

The maintenance is complete and PeerTube will be started.
