---
title: peertube down
date: 2022-12-31T17:18:28+00:00
resolved: true
resolvedWhen: 2022-12-31T18:32:22+01:00
# You can use: down, disrupted, notice
severity: down
affected:
- videos
section: issue
---
configuration error
