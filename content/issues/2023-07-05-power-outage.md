---
title: "Power outage"
date: 2023-07-05T08:00:00+00:00
resolved: true
resolvedWhen: 2023-07-05T08:45:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
- matrix chat client
- schildichat
- main website
- mastodon
- jc666.xyz
- files
- uptime kuma
- videos
- prometheus
- alertmanager
- prosody
- main website over onion
- files onion
- status onion
- i2p site
- postgres database (internal)
section: issue
---
server unreachable
