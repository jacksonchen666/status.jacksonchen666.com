---
title: server ports not opening
date: 2022-05-11T18:40:54+02:00
resolved: true
resolvedWhen: 2022-11-24T15:17:00+01:00
severity: disrupted
section: issue
affected:
- matrix chat client
- new blog
- mastodon
- logs
- files
- analytics/tracking
- videos
- uptime kuma
---
not again, subdomain services are not available because of the new router.
yes, because of a router change, for some reason, port 80 and 443 cannot be opened, except everything else basically.

because now everything on the clearnet (or \*.jacksonchen666.com) that i host is down (except for my matrix homeserver, on port 8448), well, it doesn't work.
so now the only way to access subdomain services is over tor, and not everything actually works there as of now.

that's all i really have. nothing. or, not much.

# update 2022-05-12T08:53:36+02:00 - workaround for the time being
because there appears to be no solution, the only one is to expose everything HTTPS on port 8448 instead.
as so, everything over port 443 (not 80) will be exposed to port 8448, and you'll have to specify the port by including `:8448` after something like `asdf.jacksonchen666.com` (before the `/` (slash) too).
sorry for the massive disruption.

(because things aren't working on the main https port, the status page has been modified to check on port 8448 too, so accuracy really depends on what you define as accurate or inaccurate)

# update 2022-05-13T08:17:39+02:00 - situation update
i plan to resolve this issue as soon as possible, but there are some people who are just not doing as I asked in a timely manner, so the solution might be delayed.

because of that, i've redirected port 443 to 8448, and so subdomain services are on port 8448 (there's problems with that so just be aware)

# update 2022-05-14T23:22:00+02:00 - the moment that changed everything (not really)
so i was chatting in a small private matrix room with some random people, and [@s410:matrix.org](https://matrix.to/#/@s410:matrix.org) suggested using DMZ.
i had some doubts (and didn't read the explanation yet), but tried anyways. and then requests while on the router on https worked as expected.

i should note, that it doesn't mean now port 443/80 works on the internet, but at least it works for me so i don't have to deal with a headache of relogging in everything.

# update 2022-05-17T19:22:11+02:00 - i have no solutions
yes, i'm seriously out of ideas and i don't have anymore viable solutions to opening the necessary main ports that are needed to function.
if you do have one to suggest, [please contact me via email](/contact).

what i've already tried:
- upgrading the firmware of the router (there's no firmware section)
- changing the management port for the router (there's also no settings for that)

# update 2022-05-30T19:38:42+02:00 - after some port scanning
after doing some port scanning on myself with nmap, i noticed a few things:
- some ports like 53, 80, 443 cannot be opened, and is always filtered no matter the state of the settings
- the ssh port is very wide open and works (it's a trap btw, nothing to see there then)

so i suspect that it is the router at the firmware/software doing all the blocking, and the only way to actually change the hardware is either asking the ISP or some secret way i don't know.

i tried to get it to work through upnp, and it did work for ports like 81 and 444, but not 80 and 443.

# update 2022-07-12T17:46:37+02:00 - some contact has been made
after finally weeks and months of pushing people do to things, i finally managed to get into contact with someone who could be helpful in the situation.
from talking to the people (not the contact, that's email and i was physically there, and that email was someone else), it seems like my ISP only intentionally block port 25 (no good source), which isn't applicable to me (at least not yet).
i hope to get this issue resolved soon, or i might just be stuck again.

# update 2022-07-13T11:10:12+02:00 - contact did not help
the contact with someone over email did not help.
as such, yeah there's nothing i can do now.

# update 2022-11-01T15:30:48+01:00 - asked ISP about using own router
so i decided to go the ISP and ask if changing the wifi router to something of our choice is possible, unfortunately i don't remember.
however, the ISP again, gave the same contact. hopefully it works this time.

(sidenote: i realized that i ask about Y (router change) and not talking about X (specific port not open), which is the [XY problem](https://en.wikipedia.org/wiki/XY_problem). what do you expect from the best human? well, probably still mistakes.)

# update 2022-11-24T15:17:00+01:00 - normal function has been restored
hello, after 23 days,
i bought a new router, then replaced with the old ISP bought router.

port 80/443 is now properly open, and it's all the ISP router's fault for blocking it.

anyways, ENJOY!
