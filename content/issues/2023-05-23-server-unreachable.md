---
title: server unreachable
date: 2023-05-23T09:14:27+02:00
resolved: true
resolvedWhen: 2023-05-23T22:44:11+02:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
- matrix chat client
- schildichat
- main website 
- mastodon
- files
- uptime kuma
- videos
- prometheus 
- alertmanager 
- grafana 
- postgres database (internal)
- prosody
- main website over onion 
- files onion 
- status onion
- i2p site
section: issue
---
power outage

# update 2023-05-23T22:44:11+02:00 - up again

issue resolved, server is up again.
