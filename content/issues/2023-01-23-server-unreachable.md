---
title: server unreachable
date: 2023-01-23T08:00:00+01:00
resolved: true
resolvedWhen: 2023-01-23T19:14:35+01:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- matrix homeserver
- matrix chat client
- mastodon
- files
- uptime kuma
- videos
- prometheus
- alertmanager
- grafana
- main website over onion
- files onion
- status onion
section: issue
---
# 2023-01-23T09:40:26+01:00 - time of noticed issue
due to an unknown issue, the subdomain services server is not reachable.

further information:
- the public ip responds to pings
- the server private ip address responds to pings
- the server does not respond to ssh connection attempts
- no known configuration shenanigans was done earlier
- no notification was triggered about service downtime
- the private vpn works (able to access things as expected)
- the onion services are "disconnected" (0xF2, "Introduction failed, which
  means that the descriptor was found but the service is no longer connected
  to the introduction point. It is likely that the service has changed its
  descriptor or that it is not running.")
- the public services do not connect

# update 2023-01-23T17:01:46+01:00 - further information
further information about the issue:
- ports like 443 and others are not accessible (except for non-critical
  things like port 80 and the false ssh port)
- onion service is dead, ssh access is not possible at all
- this issue probably happened earlier than the recorded date, so that will
  change
- actually, i did mess with the system. it's upgrading the system

# update 2023-01-23T19:14:35+01:00 - server is now back up
the server has been force restarted and is now back up.
things should be functional.

# event analysis - 2023-01-23T19:35:54+01:00
looking into:
- systemd logs: logs stop at 2023-01-23T08:00:02+01:00 before reboot
- prometheus: 
  this is the results of `count({__name__=~".+"})`, listed by time and
  returned result

  - 2023-01-23T07:04:52Z: 10804
  - 2023-01-23T07:04:53Z: 8217
  - 2023-01-23T07:04:55Z: 2414
  - 2023-01-23T07:04:56Z: 1336
  - 2023-01-23T07:04:58Z: 1180
  - 2023-01-23T07:04:59Z: 0

- sysrq: that didn't work, for some reason. that should've worked.
- smart data: some bad signs (reallocated sectors: 2)

from that, it's probably reasonable to guess that the root filesystem was
mounted read only (possibly due to an i/o error), and things broke.

i came to that conclusion because:
- vpn was working (does not write to disk, only reads at least)
- logs weren't spewing out
- partial functionality (responds to pings, can't serve)

however, it is not clear *why* the root filesystem became read only (and if
it's an i/o error, why was there an i/o error?)

as a measure to prevent similar scenarios, errors on the root filesystem and
swap will result in a kernel panic which automatically reboots.

(this paragraph was written at 2023-01-23T20:30:34+01:00)
