---
title: synapse down for some cleanup
date: 2022-11-29T20:34:31+01:00
resolved: true
resolvedWhen: 2022-11-29T20:44:06+01:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
section: issue
---
the matrix synapse homeserver is down for some vacuuming on the database.

it will go on for a bit.
