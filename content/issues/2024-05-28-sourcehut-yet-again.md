---
title: "SourceHut, Yet Again"
date: 2024-05-28T07:20:00Z
resolved: true
resolvedWhen: 2024-05-28T22:00:00Z
# You can use: down, disrupted, notice
severity: down
affected:
- status pages
- main website backup
- main gemini capsule backup
section: issue
---
# initial report 2024-05-28T08:59:10+00:00
Down. Yes. Sometimes. But still down.

# update 2024-05-28T13:15:00+00:00
Changed severity to disrupted level.

# resolving 2024-05-29T07:45:00+00:00
It stopped dying. Although it did continue. See [next status thingy]({{<
relref "issues/2024-05-29-sourcehut-downing-moment.md" >}})
