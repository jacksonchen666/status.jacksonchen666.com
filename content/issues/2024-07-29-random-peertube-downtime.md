---
title: "Random PeerTube Downtime"
date: 2024-07-29T17:55:00Z
resolved: true
resolvedWhen: 2024-07-29T19:50:00Z
# You can use: down, disrupted, notice
severity: disrupted
affected:
- videos
section: issue
---
# report 2024-07-29T19:30:00Z

PeerTube appears to be going down sometimes. Investigating now.

# update 2024-07-29T20:00:00Z: resolving for now

It seems like PeerTube is running fine again after a restart. We'll be
monitoring this further and come fix it again if it breaks again.
