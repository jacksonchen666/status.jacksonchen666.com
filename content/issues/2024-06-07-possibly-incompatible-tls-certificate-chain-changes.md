---
title: "Possibly Incompatible TLS Certificate Chain Changes"
date: 2024-06-07T18:30:00Z
resolved: true
resolvedWhen: 2024-06-07T19:35:00+00:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- main website
- files
- mastodon
- matrix homeserver
- prosody
- videos
- wiki
- status pages (laptop-server)
- uptime status
- calendar
- miniflux
- ntfy
- another-gts.hazmat.jacksonchen666.com
- mumble server
section: issue
---
# report 2024-06-07T19:50:00+00:00

This issue only applies to clients which do not trust the ISRG X2 root
certificate. Otherwise, it should look like nothing happened.

Unfortunately, some clients do not happen to trust that certificate. So
there's this issue.

The changes have been rolled back, so clients should work again without TLS
complaints.

## Scope

This issue can be quite specific, as it may or may not affect one or another
program. Here's a list of the conditions required to be met for the issue to
happen:
- Domain name (technically, places where the partially problematic cert is used)
- Client's lack of trust for the new ISRG X2 root certificate

This issue only applies to the following domains[^gts]:
- jacksonchen666.com
- \*.jacksonchen666.com
- \*.hazmat.jacksonchen666.com

[^gts]: This does not apply to the GoToSocial instance at
    <https://gotosocial.gts.hazmat.jacksonchen666.com>, as it also requires
    serving stuff at <https://gts.hazmat.jacksonchen666.com>, so it uses a
    different certificate entirely for just those 2 domains. That
    certificate was not affected.

The known-affected programs is very short, and extremely not exhaustive:
- ~~nheko~~
- ~~Android~~ (specifics are unknown, and is likely very specific)

These programs should be unaffected (again, extremely not exhaustive):
- Google Chrome (and anyone using their certificates trust store)
- Firefox (and anyone using their certificates trust store)
- [Apple Devices (iOS 16, iPadOS 16, macOS 13, tvOS 16, and watchOS
  9)][appletruststore]

[appletruststore]:https://support.apple.com/en-us/103100

The certificate SHA256 fingerprint was
`81:F3:BC:FC:6C:51:92:9E:B7:83:A7:54:22:AE:0C:53:29:72:00:10:09:3B:78:27:8D:2C:09:88:05:A0:8C:68`.

## Things that lead to this issue

[Let's Encrypt has deployed another root certificate: ISRG root
X2][lechainupdate]. The certificate chain using that root certificate is
also entirely ECDSA signed, which should reduce the size of the certificate
exchange.

[lechainupdate]:https://letsencrypt.org/2024/04/12/changes-to-issuance-chains.html

The linked page is a blog post which has been made a while ago. We've been
aware of that and we made changes which would apply the full ECDSA chain
upon the next certificate renewal, which happened to be today.

Unfortunately, not everything seems to have the ISRG X2 root certificate in
their certificate trust stores. This includes programs which bring their own
trust stores instead of using the system trust store.
