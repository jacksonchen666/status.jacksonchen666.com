---
title: Server major distro upgrade
date: 2023-04-22T10:00:00+02:00
resolved: true
resolvedWhen: 2023-04-22T10:20:09+02:00
# You can use: down, disrupted, notice
severity: notice
affected:
- matrix homeserver
- matrix chat client
- mastodon
- files
- uptime kuma
- videos
- prometheus
- alertmanager
- grafana
- postgres database (internal)
- main website over onion
- files onion
- status onion
- i2p site
section: issue
---
# 2023-04-19T14:58:20+02:00 - Notes and scheduling

Fedora 38 got released, so I'm going to prepare a reboot for the upgrade.

I will not be proceeding with the upgrade if the tor project still doesn't
provide packages for Fedora 38.

Dates are as follows:
- 2023-04-22T19:00:00+02:00
- 2023-04-29T19:00:00+02:00

Since [last time][last] it only took 12 minutes, I don't expect the upgrade
to take long since the packages will have been already downloaded
beforehand.

[last]: {{< relref "issues/2022-12-09-server-upgrade-planned.md" >}}

# update 2023-04-22T10:00:00+02:00 - screw it, starting update

Updating process will be started anytime soon.

# update 2023-04-22T10:20:09+02:00 - done

Done at 2023-04-22T10:20:09+02:00
