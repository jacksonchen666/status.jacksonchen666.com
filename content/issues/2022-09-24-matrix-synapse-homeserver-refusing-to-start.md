---
title: matrix synapse homeserver refusing to start
date: 2022-09-24T11:54:31+02:00
resolved: true
resolvedWhen: 2022-09-24T12:03:46+02:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
section: issue
---
hello

an update in the python package and a restart of synapse resulted in it refusing to start.
we will resolve this issue as soon as possible.

# update 2022-09-24T12:03:46+02:00 - resolved
this issue has been resolved.

the issue was something invalid in an index called "device_lists_stream_id", and a manual `REINDEX` was needed as synapse suggested.
