---
title: upcoming synapse maintenance
date: 2022-08-04T16:30:00+02:00
resolved: true
resolvedWhen: 2022-08-04T17:47:13+02:00
# You can use: down, disrupted, notice
severity: notice
affected:
- matrix homeserver
section: issue
---
hi. so i got grafana graphs going for synapse. after reviewing things more, i've decided that i'm going to do some ~~maintenance~~ changes on synapse.

the changes is to completely remake the structure of workers i have previously made after lots of reviewing the graphs.
this includes consolidating multiple workers split up by endpoints and types of things it can do (reaching 10 workers), to just multiple generic workers and a few other things instead (likely to make less than 10 workers for each different thing).

because this changes the configuration of synapse, the server will be down for a bit. however, the window of when it will be down will be made large, so that in case of non-working synapse, i still have time to fix it.

laying out the maintenance window time:
- start: 2022-08-04T16:30:00+02:00
- soft-limit end time: 2022-08-04T20:00:00+02:00
- hard-limit end time: 2022-08-04T23:00:00+02:00

updates will be pushed when there's any.

# update 2022-08-03T23:09:46+02:00 - maintenance delayed
because i ran out of time today, i will be delaying the process of changing synapse tomorrow.

# update 2022-08-04T16:56:26+02:00 - starting maintenance
the maintenance will be starting very soon, and synapse will be shutdown.
the configuration for nginx will be tested, and if it does not work, synapse start up will be delayed.
the maintenance will be over when synapse is functioning properly.

# update 2022-08-04T17:47:13+02:00 - maintenance done
the change has been made and tested, and everything seems to be working.
as such, this maintenance is declared finished.
