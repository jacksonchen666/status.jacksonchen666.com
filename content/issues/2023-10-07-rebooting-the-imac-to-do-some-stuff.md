---
title: "Rebooting the imac to Do Some Stuff"
date: 2023-10-07T21:10:00Z
resolved: true
resolvedWhen: 2023-10-07T23:30:00+02:00
# You can use: down, disrupted, notice
severity: notice
affected:
- blackbox prober
section: issue
---
expect disruptions

# End 2023-10-07T23:43:04+02:00, postmortem

We have BTRFS drives, which is included in the fstab to mount it at start
up. We also needed a [service] which does `btrfs device scan` on start up
before mounting happens to make mounting work (in the `localmount` service).

[service]:https://wiki.alpinelinux.org/wiki/Btrfs#Mount_failed

After the system update, I rebooted the machine.

Bit later, I make my crontab email me logs. I notice errors, and then
noticed the drives were not mounted.

Rebootings commence. I tried finding logs. Then I noticed the following:

The `btrfs-scan` service is supposed to run before `localmount`, both at the
`boot` level. However, the `localmount` OpenRC service was starting in the
`sysinit` level, for some reason.

The cause of the problem was starting `syslog` at `sysinit` level for early
logging (that was done quite long ago). It depends on `localmount`, so it
gets started it pretty early in the process.

The solution was to put `syslog` in the `boot` level instead of the
`sysinit` level.

Notes:
- `btrfs-scan` doesn't function at the `sysinit` level, only `boot` at
  minimum.
