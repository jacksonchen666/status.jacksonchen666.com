---
title: synapse going down for database cleanup
date: 2023-01-09T19:38:17+01:00
resolved: true
resolvedWhen: 2023-01-09T19:55:39+01:00
# You can use: down, disrupted, notice
severity: notice
affected:
- matrix homeserver
section: issue
---
