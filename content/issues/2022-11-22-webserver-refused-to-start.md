---
title: webserver refused to start
date: 2022-11-22T08:47:40+01:00
resolved: true
resolvedWhen: 2022-11-22T09:27:43+01:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- mastodon
- matrix chat 
- matrix homeserver
- files
- analytics/tracking
- uptime kuma
- videos
- main website over onion
- files onion
section: issue
---
hi

some changes were made to the webserver configuration and forgot to test and reload it.
the configuration was invalid.

i restarted the server for updates.

the webserver refused to load with the invalid configuration.
as such, service was disrupted.

this issue was noticed and fixed later.
