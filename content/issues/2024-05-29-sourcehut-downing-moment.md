---
title: "SourceHut Downing Moment"
date: 2024-05-29T06:55:00Z
resolved: true
resolvedWhen: 2024-05-29T09:45:00Z
# You can use: down, disrupted, notice
severity: disrupted
affected:
- status pages
- main website backup
- main gemini capsule backup
section: issue
---
# initial 2024-05-29T07:45:00+00:00
SourceHut pages being unreliable yet again

# update 2024-05-29T07:55:00+00:00
Severity changed to disrupted.

# resolving 2024-05-29T10:31:34+00:00
Seems to have stopped downing
