---
title: "Power Outage"
date: 2023-07-27T03:30:00Z
resolved: true
resolvedWhen: 2023-07-27T04:25:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
- matrix chat client
- schildichat
- main website
- main website (srht.site)
- status pages (srht.site)
- mastodon
- jc666.xyz
- files
- uptime monitor
- videos
- prometheus
- alertmanager
- postgres database (internal)
- prosody
- main website over onion
- files onion
- status onion
- i2p site
section: issue
---
I have no idea what happened. Everything is broken.

The server still had juice, and I've left it running.

Update 2023-07-27T04:05:00Z: The server very quickly ran out of juice and
shut itself down gracefully.

Update 2023-07-27T04:25:00Z: Power back.
