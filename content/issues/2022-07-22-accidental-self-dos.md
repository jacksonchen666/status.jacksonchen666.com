---
title: accidental self DoS
date: 2022-07-22T11:00:00+02:00
resolved: true
resolvedWhen: 2022-07-22T12:41:48+02:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- matrix homeserver
- uptime kuma
- matrix chat client
- mastodon
- files
- logs
- rsync
- analytics/tracking
- videos
section: issue
---
because of a change in DNS (switch to using unbound), there has been some extreme cases of complete network slow downs and disruptions due to the large amounts of recursive resolving.
i will attempt to resolve the large amount of DNS recursion issue, but at the time, 2 of the most DNS harsh applications (uptime kuma and matrix synapse) will be brought down and up and down and up in case it raises more issues.

# update 2022-07-22T12:41:48+02:00 - things are coming back up
things are slowly coming back up, and uptime is slowly recovering from services being "down".
everything should work now.
