---
title: heisenbridge not working
date: 2022-12-09T19:10:31+01:00
resolved: true
resolvedWhen: 2022-12-11T12:19:59+01:00
# You can use: down, disrupted, notice
severity: down
affected:
- heisenbridge
section: issue
---
NOTE: the mentioned service is not available to the public, but was logged anyways.
