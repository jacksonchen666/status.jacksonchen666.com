---
title: tor onions was down
date: 2023-02-11T07:19:01+01:00
resolved: true
resolvedWhen: 2023-02-11T12:07:11+01:00
# You can use: down, disrupted, notice
severity: down
affected:
- main website over onion
- files onion
- status onion
section: issue
---
