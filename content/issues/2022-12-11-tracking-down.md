---
title: tracking down
date: 2022-12-11T13:42:51+01:00
resolved: true
resolvedWhen: 2022-12-11T14:26:22+01:00
# You can use: down, disrupted, notice
severity: down
affected:
- analytics/tracking
section: issue
---
