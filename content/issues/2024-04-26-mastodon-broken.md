---
title: "Mastodon Broken"
date: 2024-04-26T16:10:00Z
resolved: true
resolvedWhen: 2024-04-26T16:50:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
##server
- mastodon
- mastodon onion
section: issue
---
# post-mortem 2024-04-26T16:50:00+00:00

For some reason, Ruby/Rails/Whatever couldn't find libicudata.so.73 which
was required for some reason. We fixed it by installing the `libicu73`
fedora package.
