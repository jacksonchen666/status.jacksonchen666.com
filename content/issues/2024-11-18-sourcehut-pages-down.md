---
title: "Sourcehut Pages Down"
date: 2024-11-18T18:20:00Z
resolved: true
resolvedWhen: 2024-11-18T18:55:00Z
# You can use: down, disrupted, notice
severity: down
affected:
#sourcehut pages
- status pages
- main website backup
- main gemini capsule backup
section: issue
---
# initial 2024-11-18T18:45:00+00:00

Sourcehut pages is currently down. No idea why. But it is.

# resolved 2024-11-18T19:06:10+00:00

Sourcehut pages is up again.
