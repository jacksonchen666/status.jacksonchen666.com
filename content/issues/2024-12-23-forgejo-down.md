---
title: "Forgejo Down"
date: 2024-12-23T20:30:00Z
resolved: true
resolvedWhen: 2024-12-23T21:00:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
##server
- "git (forgejo)"
section: issue
---
# post-mortem 2024-12-23T22:00:00+00:00

There was massive performance degradation in the entire server due to bots
scraping git repositories and causing expensive git operations to run
constantly. Forgejo was killed to quickly resolve that problem.

The bots seem to be have been causing performance degradation since around
T05:00Z.
