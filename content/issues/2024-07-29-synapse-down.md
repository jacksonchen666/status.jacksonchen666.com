---
title: "Synapse Down"
date: 2024-07-29T20:25:00Z
resolved: true
resolvedWhen: 2024-07-29T21:00:00Z
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
section: issue
---
# report 2024-07-29T20:30:00+00:00

Due to recent configuration changes or something, Synapse is not starting
up.

# resolved 2024-07-29T21:05:00+00:00

The issue has been resolved by rebooting the entire server. Synapse is up
again.
