---
title: "MediaWiki Down for Troubleshooting"
date: 2024-01-18T18:50:00Z
resolved: true
resolvedWhen: 2024-01-18T19:25:00Z
# You can use: down, disrupted, notice
severity: down
affected:
- wiki
section: issue
---
