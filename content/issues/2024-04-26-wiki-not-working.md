---
title: "Wiki Not Working"
date: 2024-04-26T15:55:00Z
resolved: true
resolvedWhen: 2024-04-26T16:25:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
#server
- wiki
- wiki onion
section: issue
---
# update 2024-04-26T16:25:00+00:00 - fixed
Resolved by upgrading all extensions while getting the right versions for
the MediaWiki version (1.40).
