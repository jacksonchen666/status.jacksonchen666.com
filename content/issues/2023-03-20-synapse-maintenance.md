---
title: synapse maintenance
date: 2023-03-20T00:11:26+01:00
resolved: true
resolvedWhen: 2023-03-20T00:31:11+01:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix synapse
section: issue
---
