---
title: "Synapse Down Post laptop-server Upgrade"
date: 2024-10-31T21:30:00Z
resolved: true
resolvedWhen: 2024-10-31T21:45:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
##server
- matrix homeserver
section: issue
---
# resolved 2024-10-31T21:45:00+00:00
Synapse has been fixed and is now running.
