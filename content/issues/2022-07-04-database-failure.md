---
title: database failure
date: 2022-07-04T09:04:00+02:00
resolved: true
resolvedWhen: 2022-07-04T10:30:54+02:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- matrix homeserver
- mastodon
- analytics/tracking
- videos
- postgresql database
section: issue
---
because of the matrix server using even more memory and even more connections to connect to the database (give 10 connections **per worker** ***each connection using 300 MB of RAM***), postgresql was killed.
adjustments will be made, including more swap, and less connections to the database.
it is not done yet.

to know about future failures, the postgresql database has been added to the list of services that is checked for uptime.

# update 2022-07-04T10:30:54+02:00 - bringing the system back up
the ~~system~~ was temporaily brought down so that swap changes can be made.
changes to the database has also been made.
the database has been brought back up.
