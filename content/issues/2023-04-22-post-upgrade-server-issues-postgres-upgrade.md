---
title: "Post upgrade server issue: postgresql upgrade"
date: 2023-04-22T10:21:37+02:00
resolved: true
resolvedWhen: 2023-04-22T10:58:50+02:00
# You can use: down, disrupted, notice
severity: down
affected:
- postgres database (internal)
- mastodon
- matrix homeserver
- videos
section: issue
---
