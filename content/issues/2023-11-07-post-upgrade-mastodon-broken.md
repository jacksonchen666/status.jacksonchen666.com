---
title: "Post Upgrade Mastodon Broken"
date: 2023-11-07T21:30:00Z
resolved: true
resolvedWhen: 2023-11-07T22:00:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
- mastodon
section: issue
---
Dedicated thing for Mastodon

# update 2023-11-07T22:00:00+00:00 - done

Fixed mastodon
