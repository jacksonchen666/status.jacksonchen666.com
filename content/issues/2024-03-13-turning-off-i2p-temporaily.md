---
title: "Turning Off I2P Temporaily"
date: 2024-03-13T21:15:00Z
resolved: true
resolvedWhen: 2024-03-13T22:25:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
- i2p site
section: issue
---
This is to compare performance with and without I2P.
