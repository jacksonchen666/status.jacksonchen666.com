---
title: some changes to micro needing to bring it down
date: 2022-09-24T19:03:23+02:00
resolved: true
resolvedWhen: 2022-09-24T19:50:28+02:00
# You can use: down, disrupted, notice
severity: down
affected:
- mastodon
section: issue
---
some changes will be made to mastodon which will require mastodon to be brought down, because it wouldn't stay up if the changes will be made.

sorry for the disruption, as it might take an hour or two to fully make the changes and make mastodon go run.

# update 2022-09-24T19:50:28+02:00 - still can't figure it out
i still can't figure out how to change the source code link in a faster manner. as such, i have given up again, and declaring the source code for my mastodon instance is [this](https://github.com/ClearlyClaire/mastodon/tree/fixes/webauthn-openssl-3) instead of the link provided in the mastodon instance until further notice.
