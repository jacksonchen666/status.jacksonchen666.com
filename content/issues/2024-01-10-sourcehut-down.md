---
title: "Sourcehut Down"
date: 2024-01-10T08:50:00Z
resolved: true
resolvedWhen: 2024-01-13T11:50:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
- main gemini capsule backup
- main website backup
- status pages
section: issue
---
<https://status.sr.ht/issues/2024-01-10-network-outage/>

We do not have monitoring for the affected services so we're using the
sourcehut status page time. (2024-01-10T08:40Z)

# update 2024-01-13T11:17:37+00:00 - pages.sr.ht is not up yet

pages.sr.ht is not up yet, and is what we depend on. So this outage is still
not over.

# update 2024-01-13T12:09:04+00:00 - pages.sr.ht is up but read-only

pages.sr.ht is up, but still read-only. That means things like this status
page cannot be updated if hosted on pages.sr.ht.

Services will be considered up now.
