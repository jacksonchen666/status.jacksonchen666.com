---
title: server down again
date: 2022-09-13T08:40:24+02:00
resolved: true
resolvedWhen: 2022-09-13T19:19:23+02:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
- matrix chat client
- mastodon
- files
- rsync
- analytics/tracking
- uptime kuma
- videos
- main website over onion
- onion logs
- files onion
section: issue
---
hi. i'm not sure exactly when, but the server is completely down again. i will update with the time when it actually went today when possible.

# update 2022-09-13T20:34:32+02:00 - system definitely working & some unfortunate news
hi. the server now works again. this was due to a black-out (aka power outage).
however, there are planned black-outs for the next 2 days, starting from T09:00+02:00 to T16:30+02:00 for the next 2 days from 2022-09-13.
