---
title: "disk upgrade: SSD (comes with downtime)"
date: 2022-04-09T12:00:00+02:00
resolved: true
resolvedWhen: 2022-04-09T23:35:40+02:00
severity: down
section: issue
affected:
- files
- videos
- new blog
- matrix homeserver
- matrix chat client
- mastodon (mcrblgng)
---
so, i bought a ssd.
yep, time to upgrade the server.
except, when?

well, first of all, i need to test that the SSD would work.

will it:
- fit?
- not fall out?
- break?

if all the answers are what i expected, then i will attempt to calculate the time required for transferring the things, then `dd` over the data.

i will be copying the backups just in case anything somehow goes wrong, like the current drive (old) somehow breaks.

i will update this page when necessary, except on the tor site since it relies on the exact server that i will be taking down temporarily.

estimated downtime for testing the SSD if it fits is estimated\* to be around 15-20 minutes, with 30 minutes being the max possible time to take to do so. starting around 19:53:00 +0200

# update 2022-04-04T20:25:46+02:00
it seems like i accidentally put the small screw into the large hole and it got stuck.
luckily, i finally found tweezers that managed to work, and then pulled out the screw.
that might've not been a problem if i didn't unscrew things i actually didn't have to unscrew to access the drive.

the SSD fits within the adapter (though with a bit of room left to wiggle), but the case fit has not been tested, though it does have holes that should fit and the size is just a bit thin.

i have returned the server back up. no changes has been done.

# update 2022-04-05T09:21:55+02:00 - planning and calculating the time
tl;dr: starts at 2022-04-09T12:00:00+0200, 6 hours rounded estimated time. also, please allow me for 12 hours at most (as hard limit, unless things went VERY wrong).

i use lvm on the server, and you should probably know lvm is logical and can have parts of the volume scattered over all of the disks (assuming lots of resizing with empty parts, whatever).

in case you would like to see where the physical extents are located for a physical volume, use `pvdisplay -m`. you're welcome.

anyways, the sequential write speed of the SSD is definitely faster than the HDD, so there's a bottleneck in the HDD.
however, i am unable to get a connection to the SSD and the HDD at the same time, so i'll have to use a middleman drive which can be connected to the system at the same time.
that means i'll have to transfer the data **twice** to different drives, and check if it even worked correctly.

just in case, i'll resize the lvm main partition down so i can reduce the entire partition layout to 1 gigabyte less so that it would fit (though i do have some free space on lvm that i could size it down to).

now, finally to the part of calculating the estimated downtime.

- source HDD sequential read is 100 MB/s
- middleman HDD sequential write speed is ??
- destination SSD sequential write speed is ? (likely negligible)
- actual speed between transfers is ???
- CPU bottlenecks: ???

so, i'm going to assume the system will be able to do 100 MB/s on all parts of the transfer.
the formula that i'll be using is whatever i can think of.

here's the formula: `(i*p)*2` with `i` being "size" (930 GiB) and `p` being "speed" (100 MiB/s).

the unit only matters if it's not the same, and should be made the same anyways.

the result of the formula is `19046.4` (in seconds due to speed in seconds) or `5 hours 17 minutes 26 seconds`, but that's not accounting the time it will take to swap the internal drive, which will be in the middle of the time.
so, diving by `2` to reverse the time it will take again, the estimated time for one drive to another is `9523.2` seconds AKA `2 hours 38 minutes 43 seconds`.
well, that's not so useful, because the estimated time will now be rounded to 6 hours to account for extra things i might waste time on.

now, that means it will take the entire day (basically), and i will have to do an intervention in the middle to swap the drive too, so i can't really just go to sleep because that's would cause an extended downtime.
since estimates can be wrong, i'll say that the max downtime the server should have is 10 hours at most as a hard limit, unless things went so wrong it's horrible.

finally, because the upgrade will require physical presence and an estimated rounded time of 6 hours, it will have to be done at 2022-04-09T12:00:00+0200.

tl;dr: starts at 2022-04-09T12:00:00+0200, 6 hours rounded estimated time. also, please allow me for 10 hours at most (as hard limit, unless things went VERY wrong).

# update 2022-04-06T17:13:45+02:00 - but affected services?
because the server will have to go down, all services including [files], [videos], [new blog], [matrix homeserver][matrix] & [element client][chat], and [micro] will be down at the specified time, except for [microblogging] since it is a static site hosted by <https://gitlab.com>.

# update 2022-04-06T18:21:07+02:00 - updated estimate
tldr: change (middleman drive has been decided) is negligible enough to not warrant an actual updated estimate.

since i found another drive to act as a middle man (which had a backstory on how i got it[^1]) that was pretty much empty.
after plugging it into the server and running a sequential write with `dd` with the data coming from `/dev/zero`, the result is 118 MB/s after running for a while.
that means, it might be shorter than 5 hours and 15 minutes (allowing me for extra time to figure out anything that might be wrong).
however, i haven't tested the read speed of the current internal hard drive, so it really depends.

# update 2022-04-09T20:02:41+02:00 - wrong `dd` arguments
after getting interrupted of the transfer of the data due to power loss due to me forgetting to plug the power in, i "resumed" the transfer of data.
and the reason i say "resumed" in quotes is because i forgot to seek on the output file (the drive i'm writing to, the ssd), and also offset the input file so essentially i skipped some data and then wrote it to the wrong place. and it's all absolutely useless.
i noticed it when `dd` was writing over 500GB of data, like 700GB of data that was placed in the wrong place.
because the previous transfer took 3 hours, i'm extending this downtime hard limit to 2022-04-10T00:00:00+02:00.

over 2.5 hours wasted...

# update 2022-04-09T23:35:40+02:00 - it is now working and everything has been checked
hi, after spending over 12 hours just waiting (over 2.5 hours wasted), everything seems to be working OK after forcing some filesystem checks.

everything should work.

good thing i gave myself extra time to spare cause it might've become too late (2 am)

because this event is no longer important (or rather, completed), it is unpinned. announcements will be removed from sites soon enough from the affected services that had it announced.

# credits
thanks for:
- `hwinfo` - giving me the computer model indirectly (`man 8 hwinfo`)
- crucial - [assuring me that their SSD with the right size is compatible](https://www.crucial.com/compatible-upgrade-for/aSUS/x450ln) (even though 2.5 inch SSD is actually enough information because [wikipedia](https://en.wikipedia.org/w/index.php?title=List_of_disk_drive_form_factors&oldid=1078258793), somehow)
- mom - giving me the accidentally perfect budget for buying the SSD (even though i didn't ask) & the tweezers
- an employee at the store - moving me to the right direction (like literally, i didn't understand Hungarian very well and spent too long looking at the wrong section)

[files]: https://files.jacksonchen666.com
[micro]: https://micro.jacksonchen666.com
[videos]: https://videos.jacksonchen666.com
[chat]: https://chat.jacksonchen666.com
[new blog]: https://blog.jacksonchen666.com
[matrix]: https://matrix.jacksonchen666.com
[microblogging]: https://microblogging.jacksonchen666.com
[^1]: when i was younger and more curious about things, i used the same drive on my moms computer then ran a thing. suddenly, i accidentally deleted the partitions (not the data), panicked a bit, and had to do a recovery of the data. then i might've accidentally deleted the partitions **again** and now she doesn't even really use it so now it's for something else including nothing. <sup>i just feel regret writing this.</sup>
