---
title: synapse broke on upgrade
date: 2022-09-13T20:31:21+02:00
resolved: true
resolvedWhen: 2022-09-13T20:42:22+02:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
section: issue
---
the synapse matrix homeserver went out after being restarted after an upgrade which removed support for TCP style replication workers, which seems to be what i was still using somehow.
the documentation for workers is probably very outdated (2 years?), but this issue has been quickly fixed.
