---
title: i apologize for the extended downtime
date: 2022-02-19T23:00:00+0100
severity: down
resolved: true
resolvedWhen: 2022-02-20T06:36:57+01:00
section: issue
---
since around 2022-02-19T23:00:00+0100, i shutdown many of the services and then triggered a crash to test kdump. it works.

i then waited for a very long time for fsck for some reason. 7 hours. till now, i will be force rebooting from the kdump kernel and booting it normally again.
kdump crash kernel probably never got the chance to make a memory dump because the filesystem check somehow took forever.

i apologize for waiting a very long time for it to complete and not announcing anything on this website for **7.5 hours**.
services will be returning back to normal soon.
