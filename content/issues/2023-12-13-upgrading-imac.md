---
title: "Upgrading imac"
date: 2023-12-13T19:25:00Z
resolved: true
resolvedWhen: 2023-12-13T19:30:00Z
# You can use: down, disrupted, notice
severity: notice
affected:
- blackbox prober
section: issue
---
