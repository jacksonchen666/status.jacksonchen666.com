---
title: "PostgreSQL Down"
date: 2024-04-26T15:45:00Z
resolved: true
resolvedWhen: 2024-04-26T16:10:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
#server
- mastodon
- mastodon onion
- matrix homeserver
- prosody
- videos
- miniflux
- gts.hazmat.jacksonchen666.com
section: issue
---
# post-mortem 2024-04-26T17:00:00+00:00

So the issue with PostgreSQL was the following:
- There is a file at `/usr/lib/systemd/system/postgresql.service` which is
  the systemd unit for PostgreSQL
- We used `systemctl edit --full postgresql`
- That creates a file at `/etc/systemd/system/postgresql.service`, which
  overrides the previously mentioned postgresql.service file
- We upgraded PostgreSQL from 15 to 16
- PostgreSQL 16 no longer has `postmaster` for starting PostgreSQL which is
  still the start command in `/etc/systemd/system/postgresql.service` (which
  is not updated because it's not part of a package)
- PostgreSQL cannot start, because systemd is trying to run a non-existent
  executable. Exit code 203.
