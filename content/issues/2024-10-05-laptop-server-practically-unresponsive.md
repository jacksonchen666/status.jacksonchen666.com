---
title: "laptop-server Emergency SSD Replacement"
date: 2024-10-05T19:15:00Z
resolved: true
resolvedWhen: 2024-10-06T18:20:00Z
# You can use: down, disrupted, notice
severity: down
affected:
#server
- main website
- main gemini capsule
- files
- jc6.xyz
- jc666.xyz
- mastodon
- matrix homeserver
- prosody
- reinfo wiki
- videos
- wiki
- status pages (laptop-server)
- uptime status
- i2p site
- files onion
- main website over onion
- mastodon onion
- reinfo wiki onion
- status onion
- uptime status onion
- wiki onion
- main website over yggdrasil
- calendar
- miniflux
- ntfy
- gts.hazmat.jacksonchen666.com
- another-gts.hazmat.jacksonchen666.com
- mumble server
- croc
section: issue
---
This issue was previously titled "laptop-server Practically Unresponsive",
and was renamed to "laptop-server Emergency SSD Replacement" on
2024-10-24T12:15:00+00:00.

# noted 2024-10-05T19:30:00+00:00

For some reason, laptop-server has reached load average >280.

I don't know why. Investigating.

# update 2024-10-05T19:55:00+00:00

Synapse has been killed, and services are able to respond to HTTP requests
now, reducing the load average from >420 to 75 and slightly reducing the
maxed out CPU PSI. However, this is still far from normal. Further
investigation is happening.

# update 2024-10-05T20:30:00+00:00

I have given up all attempts to figure out what's going on, and just
rebooted the server.

# update 2024-10-05T21:10:00+00:00

Currently, the server is down due to more investigation (which is very
inconclusive), but also, because there is currently issues related to nginx
timing out on startup, resulting in it not being able to start at all.

# update 2024-10-05T21:50:00+00:00

It seems like nothing has changed, and some services are in a repetitive of
restarting, and being timed out, while the server was in the range of load
average 70 in the past 5 minutes. Because of this, the server will be
"turned off again" (except for any static websites, and also wiki, which are
being selectively started up for now), and will run backups, and the
sysadmin will take a break from all this vague mess.

# note 2024-10-05T22:11:37+00:00

Note: Wiki is started, but its performance is horrible for reasons unknown
(backups?). Tor is also completely unable to start, because it is being
considered too slow by systemd, restarting it in the middle of starting up,
and therefore, never starting up.

# note 2024-10-05T23:23:34+00:00

Because of current time and backups being slow, the sysadmin will take an
even longer break for many hours now and will return tomorrow.

# update 2024-10-06T10:09:24+00:00

In an attempt to figure out what's happening, a disk I/O test is happening,
which will take a while. Services are still off, but yggdrasil site has been
marked as "operational" because it is operational.

# update 2024-10-06T12:00:00+00:00

A bonnie++ test was just done right now. The SSD performance is at its
worst, worse than the previous test. <!-- TODO: link -->

In an attempt to slightly improve performance or prevent further performance
degradation, wiki has been stopped.

# update 2024-10-06T12:05:00+00:00

Change of plans: The server will be shutdown, and an SSD migration will
happen while laptop-server is offline. Then laptop-server will come back
online with another SSD. This is going to take an unknown amount of time.

# update 2024-10-06T13:10:00+00:00

The disk data transfer has started, and is running at a rate of around 100
megabytes/second. Given the drives are 1 terabyte, it'll take 2 hours, 46
minutes, and 40 seconds to transfer all the data. If the transfer rate was
slightly worse (for a more conservative calculation), it'll take 3 hours, 5
minutes, and 11 seconds.

Please note that it only includes the data transfer part, and not the part
that also includes bootloader setup, and etc.

There is no hard time limit to this incident. The soft limit will be 4
hours.

# update 2024-10-06T13:40:00+00:00

The disk data transfer is now running at a speed of 80 MB/s (calculated on a
window of 5 seconds), and is expected to complete at around
2024-10-06T16:40:00+00:00.

# update 2024-10-06T16:00:00+00:00

Due to speed fluctuations, the calculated average speed is now 55 MB/s (in a
100 second window), which is expected to complete in around 1 hour and 15
minutes, or around 2024-10-06T17:15:00+00:00.

# update 2024-10-06T17:20:00+00:00

The data transfer was completed 10 minutes ago, and random checks of the
transferred data is happening. After that, the system will get its
bootloader and the system will be booted, hopefully resulting in a more
performant system.

# update 2024-10-06T17:25:00+00:00

laptop-server has booted with the new SSD. This will be marked resolved when
everything starts.

# update 2024-10-06T17:50:00+00:00

laptop-server has started, but is currently experiencing issues with DNS
resolving affecting updating the IP address that should be solved with time.

# resolved 2024-10-06T19:15:00+00:00

laptop-server was rebooted to fix the DNS issue. Now all services are
restored from 2024-10-06T18:20:00Z. This issue is resolved.

# post-mortem 2024-10-24T12:15:00+00:00

Very late, but nonetheless, a post-mortem about what the hell happened.

As noted in the first few updates, the load average reached in the
hundreds, which was never seen before. A more reasonable load average value
is in the range of single digits or double digits, a good rule of thumb
apparently is the amount of CPU cores in your system. A reboot didn't work
either.

So, the server was struggling to run anything at all, except for nginx and
static websites. Everything else was turned off at the time.

After doing an I/O test, it looked horrible. So a decision was made: Do an
emergency SSD replacement.

The SSD was taken out from laptop-server, put into side-desktop, both booted
into SystemRescue and a network block device was setup, running `dd` from
the old SSD to the new SSD (installed with paper padding because the metal
structure was stuck on the old SSD due to stripped screws). After that,
laptop-server was rebooted, and everything is back to normal.

Data on the old SSD has already been erased, but laptop-server performance
is somehow better now.

The old SSD also had a wear leveling of roughly 50% writes left (according
to SMART data), which is roughly at the point where everything became
abysmally slow, with a sequential read speeds of roughly 250 MB/s (even
though the advertised speeds is 560 MB/s).
