---
title: server down, just straight up down
date: 2022-06-01T13:00:00+02:00
resolved: true
resolvedWhen: 2022-06-01T17:41:32+02:00
section: issue
---
the server is straight up down.

i'll be there at around 2022-06-01T17:30+02:00, and i'll check what the server is up to.

otherwise, no services is accessible, or tor onions.

# update 2022-06-01T17:41:32+02:00 - the problem as been identified and the solution has been applied
the problem? someone took out the ethernet for the server.

that's something physical, and cannot be resolved remotely.

that's all.
