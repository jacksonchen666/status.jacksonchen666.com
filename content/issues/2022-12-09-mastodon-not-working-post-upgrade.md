---
title: mastodon not working post upgrade
date: 2022-12-09T19:11:05+01:00
resolved: true
resolvedWhen: 2022-12-09T19:49:26+01:00
# You can use: down, disrupted, notice
severity: down
affected:
- mastodon
section: issue
---
# update 2022-12-09T19:49:26+01:00 - reinstall of environment did it
reinstalling the environment stuff has fixed the issue.
