---
title: "Retranscoding Videos on PeerTube"
date: 2024-04-05T12:00:00Z
resolved: true
resolvedWhen: 2024-04-06T11:30:00+00:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- videos
section: issue
---
# late report - 2024-04-05T17:05:00+00:00

We are re-transcoding every video on the PeerTube instance since the noted
start time in the metadata. This may take up to 48 hours, and will disrupt
the usage of the PeerTube because the videos are hidden while transcoding.

The changes are:
- More lower resolutions (360p, 240p, 144p, and audio only)
- Web video (along side HLS video)

Because of those changes, we are re-transcoding every video to retroactively
apply the changes.

However, there might be some weird issues like:
- Videos list not showing videos until the video is transcoded
- Duplicate resolutions, especially the 480p resolutions

The steps to retroactively apply the changes are:
1. Transcode videos for web
2. Transcode videos in HLS

We are currently on the first step. After all videos are complete with
transcoding, the second step will be started, which will hide the videos
again.

# update 2024-04-07T22:45:00+00:00
We have moved onto step 2: Transcode videos in HLS.

This will add more lower resolution options.

Also, updating the steps with a new step:
1. Transcode videos for web
2. Transcode videos in HLS
3. Remove any duplicate resolutions

# update 2024-04-06T11:30:00+00:00
Pretty much all videos have been transcoded. This thing will be considered
over.

We will be moving onto step 3 as well, but that won't be disruptive
(hopefully).
