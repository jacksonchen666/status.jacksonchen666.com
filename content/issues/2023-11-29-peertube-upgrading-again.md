---
title: "Peertube Upgrading Again"
date: 2023-11-29T10:40:00Z
resolved: true
resolvedWhen: 2023-11-29T10:50:00+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
- videos
section: issue
---
<https://github.com/Chocobozzz/PeerTube/releases/tag/v6.0.1>

# update 2023-11-29T10:50:00+00:00 - done

Done
