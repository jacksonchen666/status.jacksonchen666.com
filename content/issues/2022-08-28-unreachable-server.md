---
title: unreachable server
date: 2022-08-28T17:19:29+02:00
resolved: true
resolvedWhen: 2022-08-28T19:50:09+02:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
- matrix chat client
- mastodon
- files
- rsync
- analytics/tracking
- uptime kuma
- main website over onion
- onion logs
- files onion
- videos
section: issue
---
# initial update 2022-08-28T18:41:24+02:00 - unreachable server
an issue has been noticed with the server.
all services do not work. this may indicate an issue at the network level.

current status:
- **all** onions do not work
- public services do not work
- host is responding to pings
- ports appear to be closed (for some reason)
- the set date and time for this post is current and not the starting time of the services being down as the exact downtime has not been determined

due to complete failure of all onion services and public services, a recovery cannot be made and services will not recover for the foreseeable future. the cause of the issue is currently still unknown.

# update 2022-08-28T19:44:56+02:00 - services should start working soon
after some remote troubleshooting, it was determined that there was a power outage for the network and the server, preventing the server from reaching the internet.
the server should work in about 5 minutes.

# update 2022-08-28T19:51:09+02:00 - mostly working state and resolved
the server is currently in a mostly working state and the issue is now declared resolved.
thanks for your patience. i guess
