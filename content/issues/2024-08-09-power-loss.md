---
title: "Power Loss"
date: 2024-08-09T08:00:00Z
resolved: true
resolvedWhen: 2024-08-09T09:00:00Z
# You can use: down, disrupted, notice
severity: down
affected:
- main website
- main gemini capsule
- files
- jc6.xyz
- jc666.xyz
- mastodon
- matrix homeserver
- prosody
- reinfo wiki
- videos
- wiki
- status pages (laptop-server)
- uptime status
- i2p site
- files onion
- main website over onion
- mastodon onion
- reinfo wiki onion
- status onion
- uptime status onion
- wiki onion
- main website over yggdrasil
- calendar
- miniflux
- ntfy
- gts.hazmat.jacksonchen666.com
- another-gts.hazmat.jacksonchen666.com
- mumble server
- croc
section: issue
---
Due to external circumstances, power has been lost. All services are down
because of that.

# resolved 2024-08-09T09:00:00+00:00
This issue has been resolved (the power is back).
