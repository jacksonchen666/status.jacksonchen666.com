---
title: "I2P Site Probably Down"
date: 2024-04-21T23:20:00Z
resolved: true
resolvedWhen: 2024-05-06T08:55:00Z
# You can use: down, disrupted, notice
severity: disrupted
affected:
- i2p site
section: issue
---
# created - 2024-04-22T11:16:29+00:00
Noticed the I2P site is apparently down, for some reason.

# update - 2024-04-22T11:48:14+00:00
With further investigation, it looks like a I2P network-wide DDoS.

We will be ignoring any issues about the I2P site for a few hours.

# non-update - 2024-04-23T12:34:05+00:00
We don't know what to do with this I2P network-wide DDoS. So we'll be doing
nothing about this.

Further status updates should not be expected until the site is fine again.

# update 2024-05-01T18:35:00+00:00
The stability of the I2P site has generally gone up, but the DDoS still
seems to be going on. Therefore, this incident level has been downgraded to
a "disrupted" level.

# setting as resolved - 2024-05-06T08:55:00+00:00
Since the stability of the I2P site has mostly been high, this incident will
be considered resolved.
