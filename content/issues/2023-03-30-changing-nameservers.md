---
title: Changing nameservers
date: 2023-03-30T11:44:27+02:00
resolved: true
resolvedWhen: 2023-03-31T16:57:00+02:00
# You can use: down, disrupted, notice
severity: notice
affected:
- main website
- status pages (srht.site)
- matrix homeserver
- matrix chat client
- files
- mastodon
- uptime kuma
- videos
- prometheus
- grafana
- alertmanager
section: issue
informational: true
---
<!--
announcement places:
mastodon
uptime.
*.sr.ht
-->

The domain for jacksonchen666.com will be changing nameservers.

The procedure appears to be:
1. Disable DNSSEC (start: 2023-03-30T11:44:27+02:00)
2. Wait for DNS propagation (duration: ~~about 48 hours~~ at least
   21000 seconds or about 6 hours as of 2023-03-30T16:45:44+02:00)
3. Change Nameservers
4. (current until the end of time) Hope things work

The services under that domain are marked as affected.

Onion services are unaffected.

# update 2023-03-30T16:45:44+02:00 - step 2 skipped, step 3 coming
I have decided that i will skip step 2, since only 1 DNS server (in the list
of DNS servers i checked) still has old stuff cached.

# update 2023-03-30T17:23:01+02:00 - nameservers changed
The nameservers has been changed.

If you experience any problems, try clearing your DNS cache.
If it's still broken, go to your DNS resolver and clear its DNS cache (with
1.1.1.1 you can (and I did)).

This should be over at 2023-03-31T16:57+02:00 (24 hours after NS change).
