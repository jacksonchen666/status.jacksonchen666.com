---
title: "Upgrading Yggdrasil to 0.5"
date: 2023-11-03T22:10:00Z
resolved: true
resolvedWhen: 2023-11-03T22:13:00+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
- main website over yggdrasil
section: issue
---
[Yggdrasil 0.5][ygg0.5] has been released, and it is incompatible with
Yggdrasil 0.4.

[ygg0.5]:https://github.com/yggdrasil-network/yggdrasil-go/releases/tag/v0.5.1

[The (Alpine Linux) PR to upgrade Yggdrasil to 0.5][alpinepr] was merged,
and the things have been updated.

[alpinepr]:https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/53938

The Yggdrasil website will also be upgraded to use Yggdrasil 0.5. The link
will not change.

This is expected to take half an hour reasonably, and 1 hour at most. It
will start at the indicated time.

# update 2023-11-03T11:15:28+00:00 - delayed

Upgrade delayed until further notice, the Alpine Linux edge repo does not
have the package yet.

# update 2023-11-03T11:38:12+00:00 - attempting later

Will be attempting later at indicated date.

# update 2023-11-03T13:30:43+00:00 - attempting much later

Still not updated. See indicated date time.

# update 2023-11-03T22:10:00+00:00 - attempting now

I will now be attempting to upgrade Yggdrasil.

# update 2023-11-03T22:14:07+00:00 - done

It is now done, and very little downtime has occurred. Basically nothing
happened.
