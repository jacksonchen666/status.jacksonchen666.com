---
title: DNS A record wrong
date: 2023-04-09T13:55:55+02:00
resolved: true
resolvedWhen: 2023-04-09T14:21:49+02:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- matrix homeserver
- matrix chat client
- mastodon
- files
- uptime kuma
- videos
- prometheus
- alertmanager
- grafana
section: issue
---
Our DNS update key expired too soon (not used for too long) 🫠

Fixed by extending unused period.
