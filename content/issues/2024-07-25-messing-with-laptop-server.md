---
title: "Messing With laptop-server"
date: 2024-07-25T22:25:00Z
resolved: true
resolvedWhen: 2024-07-25T23:05:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
- main website
- main gemini capsule
- files
- jc6.xyz
- jc666.xyz
- mastodon
- matrix homeserver
- prosody
- reinfo wiki
- videos
- wiki
- status pages (laptop-server)
- uptime status
- i2p site
- files onion
- main website over onion
- mastodon onion
- reinfo wiki onion
- status onion
- uptime status onion
- wiki onion
- main website over yggdrasil
- calendar
- miniflux
- ntfy
- gts.hazmat.jacksonchen666.com
- another-gts.hazmat.jacksonchen666.com
- mumble server
- croc
section: issue
---
# report 2024-07-25T22:45:00+00:00

We are currently messing with laptop-server and trying to check its current
battery status. This may involve unexpected shutdowns however, so downtime
can be random.

# resolved 2024-07-25T23:45:00+00:00

Marked as resolved.
