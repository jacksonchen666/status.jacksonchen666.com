---
title: "unbound DNS Was Down"
date: 2024-05-11T15:55:00Z
resolved: true
resolvedWhen: 2024-05-26T19:10:00Z
# You can use: down, disrupted, notice
severity: down
#affected:
section: issue
informational: true
---
# report 2024-05-26T21:05:00+00:00
unbound DNS resolver is a local DNS resolver, and for some reason it was
down.

The impact of services is unknown since it doesn't seem like anything went
down or broke at all. Apart from the certificate issuing. So this incident
is marked as "informational".
