---
title: "FFmpeg Broke Peertube Again"
date: 2023-09-11T18:30:00Z
resolved: true
resolvedWhen: 2023-09-11T19:08:00Z
# You can use: down, disrupted, notice
severity: disrupted
affected:
#- alertmanager
#- files
#- i2p site
#- jc6.xyz
#- jc666.xyz
#- main gemini capsule
#- main website over onion
#- main website
- mastodon
#- matrix chat client
#- matrix homeserver
#- files onion
#- postgres database (internal)
#- prometheus
#- prosody
#- schildichat
#- status onion
#- uptime monitor
- videos
#- main gemini capsule backup
#- main website (srht.site)
#- main website backup
#- status pages
section: issue
---
So, same as last time, ffmpeg doesn't run. This time, it's the same issue
(ffmpeg requires some random crap that's not available), but while the
package has been downgraded.

Solutions will be attempted.

2023-09-11T18:55Z: Updated to disrupted and included mastodon.
Mastodon does not seem to load videos correctly.

2023-09-11T19:08Z: Thanks to psykose, the issue has been resolved. It
required installing the `jack-audio-connection-kit` package.
