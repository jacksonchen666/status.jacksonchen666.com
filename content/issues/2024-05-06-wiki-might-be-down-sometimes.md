---
title: "Wiki Might Be Down Sometimes"
date: 2024-05-06T07:45:00Z
resolved: true
resolvedWhen: 2024-05-06T08:40:00+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
- wiki
- wiki onion
section: issue
---
We are testing backups, and that also involves stopping the database.
Because of the database being stopped, wiki will be unavailable sometimes or
for some duration.

# resolved 2024-05-06T08:40:00+00:00

The testing is done and the wiki will stay up now.
