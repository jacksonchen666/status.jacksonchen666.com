---
title: server not responding
date: 2023-02-07T07:46:15+01:00
resolved: true
resolvedWhen: 2023-02-07T16:18:35+01:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- matrix homeserver
- matrix chat client
- mastodon
- files
- uptime kuma
- videos
- prometheus
- alertmanager
- grafana
- main website over onion
- files onion
- status onion
section: issue
---
symptoms:
- no pings are being responded to (ip changed?)
- onion site has disconnected (no internet connection)

# update 2023-02-07T16:13:24+01:00 - services should recover soon
issue has been found, router was off.

services should recover soon, up to 30 minutes of waiting may be required
before fully recovered.

# update 2023-02-07T16:18:35+01:00 - services should've recovered
services recovered for me, thanks for waiting for an excessive amount of
time.

please clear your DNS cache if you cannot reach my subdomain servers.

# post-mortem - 2023-02-07T16:31:20+01:00
starting date was previously 2023-02-07T08:01:02+01:00, it's now
2023-02-07T07:46:15+01:00.

ending time of internet issues is 2023-02-07T16:11:00+01:00, but the ending
date was not modified.

(written at 2023-02-07T16:45:23+01:00)
after the server got internet connection, postfix proceeded to attempt to
resend some emails. that wasn't ideal for the amount of email notifications
i was getting, so i first figured out how to throw out queued up emails by
alertmanager.

(written at 2023-02-07T17:04:00+01:00)
link to see connection link being down:
<https://prometheus.jacksonchen666.com/graph?g0.expr=node_bonding_active%7Bmaster%3D%27bond0%27%7D%20%3C%202&g0.tab=0&g0.stacked=0&g0.show_exemplars=0&g0.range_input=9h&g0.end_input=2023-02-07%2015%3A30%3A00&g0.moment_input=2023-02-07%2015%3A30%3A00&g0.step_input=15>

i couldn't find anymore information. that's it.
