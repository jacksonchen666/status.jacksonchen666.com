---
title: postgres role and auth shenanigans
date: 2022-12-24T12:48:46+01:00
resolved: true
resolvedWhen: 2022-12-24T13:27:14+01:00
# You can use: down, disrupted, notice
severity: notice
affected:
- mastodon
- videos
- postgresql database (internal)
- matrix homeserver
section: issue
---
hi

learned some new things, changing postgres and everything so it works.

# update 2022-12-24T13:11:27+01:00 - restoring services
restoring services

# done 2022-12-24T13:27:14+01:00
all services restored
