---
title: resizing rootfs (with downtime)
date: 2023-01-29T16:10:00+01:00
resolved: true
resolvedWhen: 2023-01-29T19:44:10+01:00
# You can use: down, disrupted, notice
severity: notice
affected:
- matrix homeserver
- mastodon
- matrix chat client
- files
- uptime kuma
- videos
- prometheus
- alertmanager
- grafana
- postgres database (internal)
- main website over onion
- files onion
- status onion
section: issue
---
the rootfs will be resized down to limit maximum storage that can be used on
the system.

# 2023-01-29T16:10:56+01:00 - maintenance started
# 2023-01-29T19:44:10+01:00 - maintenance ended
