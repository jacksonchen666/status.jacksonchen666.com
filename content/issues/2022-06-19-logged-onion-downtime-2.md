---
title: logged onion downtime
date: 2022-06-19T23:49:00+02:00
resolved: true
resolvedWhen: 2022-06-20T00:22:00+02:00
section: issue
affected:
- main website over onion
- onion blog
- onion logs
- files onion
severity: down
---
the onion services went down, but has since been back up.

there is no record of reasoning of the event happening.
