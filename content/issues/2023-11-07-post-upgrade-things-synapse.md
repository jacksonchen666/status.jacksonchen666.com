---
title: "Post Upgrade Synapse"
date: 2023-11-07T21:30:00Z
resolved: true
resolvedWhen: 2023-11-07T21:42:29+00:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
section: issue
---
# update 2023-11-07T21:42:29+00:00 - synapse up

synapse has been fixed

# update 2023-11-07T21:50:04+00:00 - see new issue for mastodon

See new post issue for mastodon
