---
title: "Upgrading imac"
date: 2023-12-08T20:25:00Z
resolved: true
resolvedWhen: 2023-12-08T20:35:00+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
- blackbox prober
section: issue
---
Alpine Linux 3.19.0 has been released, and we expect the upgrade progress to
not take too much longer than the usual one. Should be done within about 30
minutes.

# update 2023-12-08T20:35:00+00:00 - done

Finished upgrade.
