---
title: "Attempting to Troubleshoot Random OCSP Issues"
date: 2023-09-23T09:30:00Z
resolved: true
resolvedWhen: 2023-09-23T10:30:00+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
- files
- jc6.xyz
- jc666.xyz
- main website
- mastodon
- matrix chat client
- matrix homeserver
- prometheus
- schildichat
- uptime status
- videos
section: issue
---
We've been experiencing random issues which may be coming from OCSP stapling
failing sometimes. That's the issue we suspect is happening, but we're not
sure about that.

We will be playing around in production to see what happens when things go
wrong. This may affect everyone else too, so if you notice anything wrong,
it might just be our testing. Please only report issues after our testing is
finished and the issue is persistent.

This will only affect HTTPS, not the onions or the Gemini protocol.

# end (2023-09-23T10:30:00+00:00)

We have found a way to reproduce the OCSP problem, but the solution hasn't
been found yet.

The disruption will end now.

Some notes:
- nginx reloading/testing the config makes nginx send a lot of OCSP
  requests, presumably to cache them for future use.
- `curl --cert-status` will say when OCSP is bad, so a browser and a human
  doesn't have to be part of the equation of testing anymore.
- Flushing DNS cache for the `lencr.org` zone may or may not affect the
  output, we're not sure.

# update 2024-04-05T00:00:00+00:00

We've setup [`certbot-ocsp-fetcher`][cof] to externally manage OCSP
responses (also see `ssl_stapling_file` nginx config). This seems to have
improved the reliability of OCSP stapling, especially when nginx is
reloaded.

This means there should be less random TLS errors popping up now. The amount
might be reduced to 0 as well.

[cof]:https://github.com/tomwassenberg/certbot-ocsp-fetcher
