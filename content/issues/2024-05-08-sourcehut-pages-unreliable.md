---
title: "Sourcehut Pages Unreliable"
date: 2024-05-08T23:15:00Z
resolved: true
resolvedWhen: 2024-05-09T08:15:00Z
# You can use: down, disrupted, notice
severity: disrupted
affected:
- status pages
- main website backup
- main gemini capsule backup
section: issue
---
# initial report 2024-05-09T07:00:00+00:00
We've received some alerts about sourcehut pages being unreliable. And by
"some alerts" I mean 43 emails.

We don't know why this is happening. But it is happening, so this incident
is being recorded.

# update 2024-05-09T08:35:00+00:00 - downtime may have stopped
We noticed that sourcehut pages started working again. We'll mark this as
resolved unless there's further downtime.
