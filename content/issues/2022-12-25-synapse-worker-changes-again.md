---
title: synapse worker changes again
date: 2022-12-25T15:31:15+01:00
resolved: true
resolvedWhen: 2022-12-25T20:40:36+01:00
# You can use: down, disrupted, notice
severity: notice
affected:
- matrix homeserver
section: issue
---
due to some issues, this was set as resolved later rather than when it was done.
