---
title: server not responding
date: 2023-02-21T12:01:25+01:00
resolved: true
resolvedWhen: 2023-02-21T12:13:51+01:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- matrix homeserver
- matrix chat client
- mastodon
- files
- uptime kuma
- videos
- prometheus
- alertmanager
- grafana
- main website over onion
- files onion
- status onion
section: issue
---
due to some maintenance, the router will be affected by being disconnected
from power.

amount time is unknown.

# 2023-02-21T12:13:51+01:00 - maintenance is done
maintenance is done
