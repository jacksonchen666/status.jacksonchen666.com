---
title: server down for network changes
date: 2022-11-24T12:49:41+01:00
resolved: true
resolvedWhen: 2022-11-24T15:17:00+01:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
- matrix chat client
- rsync
- files
- mastodon
- uptime kuma
- analytics/tracking
- videos
- main website over onion
- files onion
section: issue
---
the server is down because the router has been taken away preventing internet access, which will be changed soon.

this is related to the server ports issue, except that the router in question of the issue is being taken away and then being replaced with another router which hopefully will fix the issue of port 80/443 not opening on the router.

soft limit is at 2022-11-25T00:00:00+01:00, hard limit is at 2022-11-26T00:00:00+01:00.

and after that, we should be up with a new router without the issues of port 80/443 not opening, restoring easy accessibility of my services again after **6 months**.

# update 2022-11-24T14:10:06+01:00 - router has been bought
after some long decision making and purchasing other things, everything i need has been got including the new router.
i will be setting up the new router to function, and then also setup the rest of the server, which should have it's port 443 open hopefully.

# update 2022-11-24T15:24:22+01:00 - IT WORKS!!!!!!!
IT WORKS!!!!!!!

i have setup the router, and opening port 80/443 works on the outside. i have tested it.

finally, i can stop being unavailable.
i will be updating the main post of the issue.
