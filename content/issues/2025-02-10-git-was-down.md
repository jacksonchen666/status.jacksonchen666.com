---
title: "Git Was Down"
date: 2025-02-10T09:00:00Z
resolved: true
resolvedWhen: 2025-02-10T09:30:00Z
# You can use: down, disrupted, notice
severity: down
affected:
- git (forgejo)
section: issue
---
Exactly the same as [last time]({{< relref "2025-01-22-git-was-down" >}}).
