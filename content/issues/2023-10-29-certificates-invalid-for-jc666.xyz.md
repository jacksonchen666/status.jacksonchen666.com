---
title: "Certificates Invalid for jc666.xyz"
date: 2023-10-29T15:35:00Z
resolved: true
resolvedWhen: 2023-10-29T15:45:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
- jc666.xyz
section: issue
---
Did some testing and oops!
