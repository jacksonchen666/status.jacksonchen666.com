---
title: "Router Reboot"
date: 2023-09-18T10:10:00Z
resolved: true
resolvedWhen: 2023-09-18T10:15:00+00:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- alertmanager
- files
- i2p site
- jc6.xyz
- jc666.xyz
- main gemini capsule
- main website over onion
- main website
- mastodon
- matrix chat client
- matrix homeserver
- files onion
- postgres database (internal)
- prometheus
- prosody
- schildichat
- status onion
- uptime monitor
- videos
section: issue
---
