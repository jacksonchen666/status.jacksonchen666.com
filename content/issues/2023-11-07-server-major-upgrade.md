---
title: "Server Major Upgrade"
date: 2023-11-07T20:35:00Z
resolved: true
resolvedWhen: 2023-11-07T21:30:00Z
# You can use: down, disrupted, notice
severity: notice
affected:
- alertmanager
- files
- i2p site
- jc6.xyz
- jc666.xyz
- main gemini capsule
- main website over onion
- main website
- mastodon
- matrix chat client
- matrix homeserver
- files onion
- postgres database (internal)
- prometheus
- prosody
- schildichat
- status onion
- uptime status
- videos
section: issue
aliases:
- /issues/2023-10-24-server-major-upgrade/
- /issues/2023-11-04-server-major-upgrade/
- /issues/2023-11-11-server-major-upgrade/
---
# prep info - 2023-10-10T21:13:23+00:00
Based on [the Fedora 39 schedule][schedule], there will be Fedora 39 on
2023-10-24.

[schedule]:https://fedorapeople.org/groups/schedule/f-39/f-39-key-tasks.html

Here is times of upgrade:
- ~~2023-10-24T17:00:00Z~~

Just single time frame. That's it. On that time, the server restarts (as the
packages would've been prepared already).

Last time after the upgrade, it took 20 minutes and some stuff broke.

So, this upgrade will be allocated the normal time 30 minutes, and maximum
time of 1 hour. Anything more than the maximum time is "something has gone
really wrong" priority.

Other issues may arise, but I'm not sure what. If issues do arise, a
separate issue thingy will be made instead of using this.

This page will be updated as necessary. Please ignore the "this issue is not
resolved" until it becomes an effective thing.

# update 2023-10-20T19:05:53+00:00

The [schedule] has changed its final date target. The new date is
2023-10-31.

Here's the potential times of upgrade:
- ~~2023-10-31T17:00:00Z~~
- ~~2023-11-03T17:00:00Z~~
- ~~2023-11-04T17:00:00Z~~

These are just potential times currently. Most likely, the last one will be
the chosen time.

# update 2023-10-27T21:42:41+00:00

The [schedule] has changed **AGAIN**. New (potential) times:
- 2023-11-07T17:00:00Z
- 2023-11-10T17:00:00Z
- 2023-11-11T17:00:00Z

Again, it's probably going to be the last one.

# update 2023-11-07T15:44:16+00:00 - new potential starting time

Fedora 39 released, and today I might be able to upgrade later today.

Yes, this is a significant risk that I'm willing to take (or willing to take
blindly, more realistically).

Potential times:
- 2023-11-07T18:00:00Z
- 2023-11-07T19:00:00Z

The start of the upgrade will be announced, and the upgrade may take up to
an hour within reasonable time, or 2 hours max. Anything more is probably
"oh fuck everything went wrong".

# update 2023-11-07T20:35:00+00:00 - starting

I will now be starting the upgrade thing. Things will be shutdown first,
then a backup will be taken, then the rest.

# update 2023-11-07T21:32:06+00:00 - upgrade done

Upgrade done.
