---
title: Server unresponsive
date: 2023-04-12T18:00:00+02:00
resolved: true
resolvedWhen: 2023-04-12T22:00:00+02:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
- matrix chat client
- mastodon
- files
- uptime kuma
- videos
- main website over onion
- files onion
- status onion
- prometheus
- alertmanager
- grafana
- postgres database (internal)
section: issue
---
- No functioning self hosted services
- Onion down
- No characters from physical keyboard
- Able to use sysrq
- Replies to ping
- No disk activity?

The server will be force rebooted (extra details: command sequence of sysrq
commands: reisub).

# end 2023-04-12T22:00:00+02:00
End, server is back up.

# addition 2023-04-12T22:15:06+02:00 - root cause analysis
- Server went down at around T18:00+02:00
- Backups are taken every 2 hours (e.g. T00:00+02:00, T02:00+02:00,
  T04:00+02:00, etc.)
- Backups usually mess with LVM to create a consistent snapshot of `/`
- `journalctl` returns the following before next boot:

  ```
  Apr 12 18:00:01 laptop-server sh[368435]: + lvcreate -l 50%FREE -s root -n /dev/hat/root-backup_staging
  Apr 12 18:00:01 laptop-server kernel: dm-4: detected capacity change from 197124096 to 0
  Apr 12 18:00:01 laptop-server systemd-homed[880]: block device /sys/devices/virtual/block/dm-4 has been removed.
  Apr 12 18:00:01 laptop-server systemd-homed[880]: block device /sys/devices/virtual/block/dm-4 has been removed.
  ```
- The created volume is still there

Conclusion: The server started the backup, messed with LVM, and something
went wrong that broke the entire server.
