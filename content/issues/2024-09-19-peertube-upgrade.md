---
title: "PeerTube Upgrade"
date: 2024-09-19T01:45:00Z
resolved: true
resolvedWhen: 2024-09-19T02:20:00+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
- videos
section: issue
---
# notice 2024-09-19T01:45:00+00:00

PeerTube is being upgraded currently.

There is a migration currently running preventing the starting of PeerTube,
which will take a while to run while system backups are also running.

# update 2024-09-19T02:03:50+00:00

Startup migration complete, now doing the second part of migration.

PeerTube should be available now, and this part of the migration will likely
take a while.

# resolved 2024-09-19T02:20:00+00:00

Migration finished. PeerTube running pretty much normally.
