---
title: "Main Website Down"
date: 2024-01-02T11:45:00Z
resolved: true
resolvedWhen: 2024-01-02T12:20:00Z
# You can use: down, disrupted, notice
severity: down
affected:
- main website
section: issue
---
Problem: ddclient for some reason got an empty IP address again. This has
only affected jacksonchen666.com and not any of the subdomains.

This issue has already been resolved.
