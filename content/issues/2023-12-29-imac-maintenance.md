---
title: "imac Maintenance"
date: 2023-12-29T12:30:00Z
resolved: true
resolvedWhen: 2023-12-29T14:50:00Z
# You can use: down, disrupted, notice
severity: notice
affected:
- blackbox prober
section: issue
---
