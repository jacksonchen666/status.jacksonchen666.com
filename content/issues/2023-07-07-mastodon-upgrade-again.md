---
title: "Mastodon upgrade, again"
date: 2023-07-07T20:55:00+00:00
resolved: true
resolvedWhen: 2023-07-07T21:30:00+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
- mastodon
section: issue
---
<https://github.com/mastodon/mastodon/releases/tag/v4.1.4>

Mastodon will be down while upgrade
