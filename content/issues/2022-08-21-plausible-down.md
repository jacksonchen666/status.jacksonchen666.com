---
title: plausible down
date: 2022-08-21T00:31:14+02:00
resolved: true
resolvedWhen: 2022-08-21T01:42:10+02:00
# You can use: down, disrupted, notice
severity: down
affected:
- analytics/tracking
section: issue
---
# update 2022-08-21T01:22:41+02:00 - working on plausible being down
a recent monitoring notification has got me working on plausible being down. it appears to be [this matching issue](https://github.com/plausible/analytics/discussions/2043), however the server is being restarted because of it being overloaded (think 9 load average on a 4 thread), but this issue will be continued to be worked on in the meanwhile.

# update 2022-08-21T01:42:10+02:00 - issue not well pin pointed, issue resolved
after a server reboot, the issue basically disappeared. because the issue was gone before it was well pointed to exactly what, the best i can suspect is that the disk was so overloaded that the database things timed out.
