---
title: "SourceHut Down Again"
date: 2024-01-13T12:55:00Z
resolved: true
resolvedWhen: 2024-01-14T11:40:00Z
# You can use: down, disrupted, notice
severity: down
affected:
- main gemini capsule backup
- main website backup
- status pages
section: issue
---
sourcehut got DDoSed again

# update 2024-01-14T11:40:00+00:00 - back

it's back again
