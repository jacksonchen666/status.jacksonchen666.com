---
title: server unreachable
date: 2023-05-24T08:00:00+02:00
resolved: true
resolvedWhen: 2023-05-24T16:20:00+02:00
# You can use: down, disrupted, notice
severity: down
affected:
- matrix homeserver
- matrix chat client
- schildichat
- main website
- mastodon
- jc666.xyz
- files
- uptime kuma
- videos
- prometheus
- alertmanager
- grafana
- postgres database (internal)
- prosody
- main website over onion
- files onion
- status onion
- i2p site
section: issue
---
probably power outage

# update 2023-05-24T16:56:50+02:00 - back up

stuff is back up again
