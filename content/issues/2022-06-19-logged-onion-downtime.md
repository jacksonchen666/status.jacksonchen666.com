---
title: logged onion downtime
date: 2022-06-19T07:46:00+02:00
resolved: true
resolvedWhen: 2022-06-19T08:41:00+02:00
section: issue
affected:
- main website over onion
- onion blog
- onion logs
- files onion
severity: down
---
the onion services went down, but has since been back up.

there is no record of reasoning of the event happening.
