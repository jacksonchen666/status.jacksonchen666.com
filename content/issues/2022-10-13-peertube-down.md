---
title: peertube down
date: 2022-10-13T11:46:02+02:00
resolved: true
resolvedWhen: 2022-10-13T11:56:03+02:00
# You can use: down, disrupted, notice
severity: down
affected:
- peertube
section: issue
---
hi

changes were made to peertube that made it unable to continue to start up

the issue was fixed and this was noted in status

that's all
