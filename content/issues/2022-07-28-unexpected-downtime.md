---
title: unexpected downtime
date: 2022-07-27T15:15:59+02:00
resolved: true
resolvedWhen: 2022-07-28T14:08:54+02:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- mastodon
- matrix homeserver
- uptime kuma
- videos
section: issue
---
an unexpected issue has occurred in the server that prevented the server from notifying the failure of itself and the functionality of said services, and may have also affected all the services from functioning as intended.
it might've been due to issues related to DNS as uptime kuma got `getaddrinfo ENOTFOUND micro.jacksonchen666.com` error when trying to ping microblogging (and a lot of other stuff too), but the unbound dns server was functioning during the downtimes of all the other services. as such, the cause of the issue is not quite clear.

this issue was not noticed until the matrix homeserver did not return any new messages from federated rooms for more than 12 hours.

the exact services that became non-functional is not known and is assumed.

the problem was resolved with rebooting the server. all services are now up

update 2022-07-28T14:35:22+02:00: attempts to prevent further issues of notifications not going through was made by using an ip address to reach the notification service ntfy (self hosted).
