---
title: "All HTTP Down"
date: 2025-02-15T10:40:00Z
resolved: true
resolvedWhen: 2025-02-15T10:45:00+00:00
# You can use: down, disrupted, notice
severity: down
affected:
#server
- main website
- files
- jc6.xyz
- jc666.xyz
- mastodon
- matrix homeserver
- prosody
- videos
- wiki
- git (forgejo)
- status pages (laptop-server)
- i2p site
- files onion
- main website over onion
- mastodon onion
- status onion
- uptime status onion
- wiki onion
- main website over yggdrasil
- calendar
- miniflux
- ntfy
- gts.hazmat.jacksonchen666.com
- another-gts.hazmat.jacksonchen666.com
section: issue
#informational: false
---
# initial 2025-02-15T10:40:00+00:00

Due to an apparent upgrade mishap that happened with nginx, it's down.

# resolved 2025-02-15T10:45:00+00:00

This was quickly resolved. It appears that a module that was previously
compiled was not compiled anymore, and so was stuck on the old version,
preventing nginx from starting successfully.

# update 2025-02-15T11:00:00+00:00

The affected services list has been expanded as I2P, onion, and yggdrasil
sites also depends on nginx.
