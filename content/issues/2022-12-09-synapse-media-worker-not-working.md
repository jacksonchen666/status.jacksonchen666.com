---
title: synapse media worker not working
date: 2022-12-09T19:10:32+01:00
resolved: true
resolvedWhen: 2022-12-10T23:34:05+01:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- matrix homeserver
section: issue
---
# timeline
## 2022-12-09T19:10:32+01:00 - server resumes normal operation after upgrading
as known already, the server was upgraded on 2022-12-09 from fedora 36 to 37.

however, this broke a number of environments because python has upgraded.

the issue was fixed for everything, except the media worker, which kept restarting because one decadency was not installed in the environment.

## 2022-12-10T23+01:00 - i notice something is wrong, cannot upload media
1. i can't upload media
2. i check schildichat dev tools then i get 502 in devtools networks stuff
3. i check nginx logs
4. nginx error logs says upstream (for media worker) is down, returning a 502
5. i check if media worker is alive (no)
6. i check the logs for media worker (dependency not found)
7. i fix the issue
8. i write about this issue i noticed

in total, the media worker was restarted 16309 times.
yes. the issue was not noticed until now.
