---
title: wrong ip address on subdomains
date: 2023-01-08T03:39:15+01:00
resolved: true
resolvedWhen: 2023-01-08T14:24:15+01:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- matrix homeserver
- uptime kuma
- files
- mastodon
- matrix chat client
- prometheus
- grafana
- alertmanager
section: issue
---
services was unavailable because the server forgot to update it's IP
address.
