---
title: "Upgrading laptop-server"
date: 2024-10-31T20:50:00Z
resolved: true
resolvedWhen: 2024-10-31T21:30:00+00:00
# You can use: down, disrupted, notice
severity: notice
affected:
#server
- main website
- main gemini capsule
- files
- jc6.xyz
- jc666.xyz
- mastodon
- matrix homeserver
- prosody
- reinfo wiki
- videos
- wiki
- status pages (laptop-server)
- uptime status
- i2p site
- files onion
- main website over onion
- mastodon onion
- reinfo wiki onion
- status onion
- uptime status onion
- wiki onion
- main website over yggdrasil
- calendar
- miniflux
- ntfy
- gts.hazmat.jacksonchen666.com
- another-gts.hazmat.jacksonchen666.com
- mumble server
- croc
section: issue
---
# initial 2024-10-31T20:50:00+00:00

Hi since [Fedora 41][f41] an upgrade happen now

[f41]:https://fedoramagazine.org/announcing-fedora-linux-41/

Thanks for read

Give 30 minutes

# update 2024-10-31T21:05:00+00:00

The updating has been started now

# resolved 2024-10-31T21:30:00+00:00

OK it's done

Other stuff will get its own incidents if any
