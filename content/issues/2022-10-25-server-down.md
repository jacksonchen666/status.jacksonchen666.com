---
title: server down
date: 2022-10-25T23:23:39+02:00
resolved: true
resolvedWhen: 2022-10-26T00:39:55+02:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- matrix homeserver
- matrix chat client
- mastodon
- files
- rsync
- analytics/tracking
- uptime kuma
- videos
- main website over onion
- files onion
section: issue
---
