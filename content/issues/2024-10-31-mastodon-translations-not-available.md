---
title: "Mastodon Translations Not Available"
date: 2024-10-31T21:30:00Z
resolved: true
resolvedWhen: 2024-10-31T22:45:00+00:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
#server
- mastodon
- mastodon onion
section: issue
---
# issue recorded 2024-10-31T22:30:00+00:00

This issue has just now been logged

Due to python updates and discoveries about installing libretranslate
without having to resort to modifications, it is still in progress

# update 2024-10-31T22:45:00+00:00

This issue has now been fixed.
