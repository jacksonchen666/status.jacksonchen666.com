---
title: new blog has been removed
date: 2022-07-03T15:28:10+02:00
# You can use: down, disrupted, notice
severity: down
affected:
- new blog
- onion blog
section: issue
informational: true
---
after a month announcing the deprecation, new blog will be brought down and will not continue to function anymore.
the blog subdomain will now be a placeholder of "no services found".
