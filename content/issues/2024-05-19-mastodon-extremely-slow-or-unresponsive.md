---
title: "Mastodon Extremely Slow or Unresponsive"
date: 2024-05-18T22:35:00Z
resolved: true
resolvedWhen: 2024-05-18T23:00:00Z
# You can use: down, disrupted, notice
severity: down
affected:
- mastodon
- mastodon onion
section: issue
---
# created 2024-05-18T22:51:27+00:00

Due to a popular post, the mastodon instance is dying.

Sorry, don't really know what to do.

# resolved 2024-05-18T23:00:00+00:00 - hopefully over

The fire should be over now. Hopefully.
