---
title: minor cert issue with prosody
date: 2023-05-07T00:00:03+02:00
resolved: true
resolvedWhen: 2023-05-07T14:51:00+02:00
# You can use: down, disrupted, notice
severity: disrupted
affected:
- prosody
section: issue
---
# update 2023-05-07T14:51:00+02:00 - post mortem

1. certs permissions were messed with a little bit
2. prosody was unable get certs
3. prosody delivered completely different certs
4. prosody is now delivering untrusted certs

note: issue could've been persisting for much longer before the issue was
noticed, so starting date is noticed date.


# update 2023-05-07T15:00:49+02:00 - updating starting time

found some logs, updating starting time to reflect time of issue

original: 2023-05-07T14:44:14+02:00

events:
1. systemd reloads (for some reason) the prosody service
2. error in logs, unable to access certs.
3. prosody now delivering whatever cert it decided to
