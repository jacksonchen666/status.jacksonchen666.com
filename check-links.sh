#!/bin/bash
# Copyright 2023 JacksonChen666
#
# SPDX-License-Identifier: MIT
set -eu

rm -r ./public/
hugo 1>&2
# exclude 400 and 500 status codes and exclude other error for re-check
# requires moreutils's sponge
# sponge command check
# https://stackoverflow.com/questions/592620/how-can-i-check-if-a-program-exists-from-a-bash-script/#677212
if command -v sponge &> /dev/null; then
    echo "Removing negative results..." >&2
    grep -v ',[45]..' .lycheecache | grep -v ',,' | sponge .lycheecache
fi
lychee \
    --cache \
    --max-cache-age 7d \
    --scheme "https" \
    --scheme "http" \
    --exclude "(.{56}|.{16})\.onion" \
    --exclude ".*\.i2p" \
    --max-concurrency 16 \
    $(find public -type f -name \*.html)
# onion and i2p not checkable
