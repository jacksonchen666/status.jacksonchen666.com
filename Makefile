SRCFILES = ./content/** ./layouts/** ./static/** ./config/_default/config.yml

deploy_variants = onion laptop-server
hugo_variants = onion laptop-server
# e.g. public-onion
hugo_variants := $(patsubst %,public-%,$(hugo_variants))
# e.g. public-onion.tar.gz
hugo_variants_gzipped = $(patsubst %,%.tar.gz,$(hugo_variants))
all_tar_gz = public.tar.gz $(hugo_variants_gzipped)

public: $(SRCFILES)
	hugo --destination "public"

$(hugo_variants): public-%: $(SRCFILES) ./config/%/config.yml
	a="$@"; a="$${a/public-}"; echo "$$a"; hugo --environment "$$a" --destination "$@"

public.tar.gz: public
	if [ -z "$$NO_COMPRESS" ]; then ./scripts/compress-and-remove-diff-less-than-percentage.sh "public"; fi
	tar -C "public" -cz . > "public.tar.gz"

$(hugo_variants_gzipped): site_dir = $(patsubst %.tar.gz,%,$@)
$(hugo_variants_gzipped): %.tar.gz: %
	if [ -z "$$NO_COMPRESS" ]; then ./scripts/compress-and-remove-diff-less-than-percentage.sh "$(site_dir)"; fi
	tar -C "$(site_dir)" -cz . > "$@"

.PHONY: all pack-all
all: public $(hugo_variants)
pack-all: $(all_tar_gz)

.PHONY: deploy-all deploy-srht $(deploy_variants)
# e.g. deploy-onion
deploy_variants := $(patsubst %,deploy-%,$(deploy_variants))
deploy-srht: domain = status.jacksonchen666.com
deploy-onion deploy-laptop-server: domain = status2.jacksonchen666.com
$(deploy_variants): site_type = $(patsubst deploy-%,%,$@)

DEPLOY_SSH_HOST ?= s.jacksonchen666.com
DEPLOY_SSH_USERNAME ?= deploy
DEPLOY_SSH_OPTIONS ?= -o VerifyHostKeyDNS=yes
deploy-all: deploy-srht $(deploy_variants)
deploy-srht: public.tar.gz site-config.json
	hut pages publish --site-config ./site-config.json -d "$(domain)" public.tar.gz
$(deploy_variants): deploy-%: public-%.tar.gz
	ssh $(DEPLOY_SSH_OPTIONS) $(DEPLOY_SSH_USERNAME)@$(DEPLOY_SSH_HOST) -T "$(domain)::$(site_type)" < public-$(site_type).tar.gz

.PHONY: clean clean-public clean-tar.gz
clean: clean-public clean-tar.gz
clean-public:
	rm -rf public $(hugo_variants)
clean-tar.gz:
	rm -rf $(all_tar_gz)

# suitable for a command like this:
# make live &
.PHONY: live
live:
	hugo serve --navigateToChanged 2>&1 > /dev/null

.PHONY: live-production
live-production:
	hugo serve --environment production --navigateToChanged 2>&1 > /dev/null
