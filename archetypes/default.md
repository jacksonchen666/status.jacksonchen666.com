---
{{ $name := replaceRE `^\d{4}-\d{2}-\d{2}-` "" .Name 1 -}}
{{ $name = title $name -}}
{{ $name = replace $name "-" " " -}}
{{ $name = replace $name "Imac" "imac" -}}
{{ $name = replace $name "Laptop Server" "laptop-server" -}}
title: "{{ $name }}"
date: {{ (time (.Date)).UTC.Format "2006-01-02T15:04:05Z0700" }}
resolved: false
#resolvedWhen:
# You can use: down, disrupted, notice
severity: down
affected:
{{ range $.Site.Params.Categories -}}
{{- $category := .name -}}
##{{ .name }}
{{ range where $.Site.Params.Systems "category" $category -}}
#- {{ .name }}
{{ end -}}
{{ end -}}
section: issue
#informational: false
---
